FROM nikolaik/python-nodejs

# Setup assembler.
WORKDIR /app/assembler
COPY assembler/requriments.txt .
RUN pip install -r requriments.txt
COPY assembler .

# Setup docxgen.
WORKDIR /app/docxgen
COPY docxgen/package.json .
RUN npm install 
COPY docxgen .

# Run the api.
WORKDIR /app/assembler
EXPOSE $PORT
CMD python server.py --port $PORT --docxgen-dir /app/docxgen

