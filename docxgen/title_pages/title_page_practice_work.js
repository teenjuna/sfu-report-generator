// Example how to display page numbers
// Import from 'docx' rather than '../build' if you install from npm
import * as fs from "fs";
import {
	Document,
	BorderStyle,
	HeadingLevel,
	Packer,
	Paragraph,
	TextRun,
	convertMillimetersToTwip,
    AlignmentType,
    convertInchesToTwip,
    Footer,
    Media,
    TabStopPosition,
    UnderlineType,
	Table,
	TableCell,
	TableRow,
	WidthType,
	VerticalAlign
} from "docx"

export function title_practic_work(institute, department, theme, group, record_book_number, student_name, teacher_name){
	function title_1(){
		return [new Paragraph({
			text: "Федеральное государственное автономное",
			style: "BaseStyle",
			alignment: AlignmentType.CENTER,
		}),
		new Paragraph({
			text: "образовательное учреждение",
			style: "BaseStyle",
			alignment: AlignmentType.CENTER,
		}),
		new Paragraph({
			text: "Высшего образования ",
			style: "BaseStyle",
			alignment: AlignmentType.CENTER,
		}),
		new Paragraph({
			text: "«СИБИРСКИЙ ФЕДЕРАЛЬНЫЙ УНИВЕРСИТЕТ» ",
			style: "BaseStyle",
			alignment: AlignmentType.CENTER,
		})];
	}

	const TitleTableThree = new Table({
		alignment: AlignmentType.CENTER,
		rows: [
			new TableRow({
				//первый ряд
				children: [
					new TableCell({
						//первая клетка первого ряда
						columnSpan: 2,
						children: [
							new Paragraph({
								text: "Преподаватель",
								style: "BaseStyle",
								alignment: AlignmentType.LEFT,
							})
						],
						borders: {
							top: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							bottom: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(80),
							type: WidthType.DXA,
						},
					}),
					new TableCell({
						//вторая клетка первого ряда
						children: [],
						borders: {
							top: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							bottom: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
							   style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(5),
							type: WidthType.DXA,
						},
					}),
					new TableCell({
						//третья клетка первого ряда
						children: [],
						borders: {
							top: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							bottom: {
								style: BorderStyle.SINGLE,
								size: 1,
								color: "black",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(30),
							type: WidthType.DXA,
						},
					}),
					new TableCell({
						//четвёртая клетка первого ряда
						children: [],
						borders: {
							top: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							bottom: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(5),
							type: WidthType.DXA,
						},
					}),
					new TableCell({
						//пятая клетка первого ряда
						children: [
							new Paragraph({
								text: teacher_name,
								style: "BaseStyle",
								alignment: AlignmentType.CENTER,
							})
						],
						borders: {
							top: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							bottom: {
								style: BorderStyle.SINGLE,
								size: 1,
								color: "black",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(50),
							type: WidthType.DXA,
						},
					})
				]
			}),
			new TableRow({
				//второй ряд
				children: [
					new TableCell({
						//первая клетка второго ряда
						children: [],
						borders: {
							top: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							bottom: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						columnSpan: 2,
						width: {
							size: convertMillimetersToTwip(80),
							type: WidthType.DXA,
						},
					}),
					new TableCell({
						//вторая клетка второго ряда
						children: [],
						borders: {
							top: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							bottom: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(5),
							type: WidthType.DXA,
						},
					}),
					new TableCell({
						//третья клетка второго ряда
						children: [
							new Paragraph({
								text: "подпись, дата",
								style: "BaseTinyStyle",
								alignment: AlignmentType.CENTER,
								verticalAlign: VerticalAlign.TOP,
							}),
						],
						borders: {
							top: {
								style: BorderStyle.SINGLE,
								size: 1,
								color: "black",
							},
							bottom: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(30),
							type: WidthType.DXA,
						},
					}),
					new TableCell({
						//четвёртая клетка второго ряда
						children: [],
						borders: {
							top: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							bottom: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(5),
							type: WidthType.DXA,
						},
					}),
					new TableCell({
						//пятая клетка второго ряда
						children: [
							new Paragraph({
								text: "инициалы, фамилия",
								style: "BaseTinyStyleStyle",
								alignment: AlignmentType.CENTER,
								verticalAlign: VerticalAlign.TOP,
							})
						],
						borders: {
							top: {
								style: BorderStyle.SINGLE,
								size: 1,
								color: "black",
							},
							bottom: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(50),
							type: WidthType.DXA,
						},
					})
				]
			}),
			new TableRow({
				//третий ряд
				children: [
					new TableCell({
						//первая клетка третьего ряда
						children: [
							new Paragraph({
								text: "Студент",
								style: "BaseStyle",
								alignment: AlignmentType.LEFT,
							})
						],
						borders: {
							top: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							bottom: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(25),
							type: WidthType.DXA,
						},
					}),
					new TableCell({
						//вторая клетка третьего ряда
						children: [
							new Paragraph({
								text: group + ", " + record_book_number,
								style: "BaseStyle",
								alignment: AlignmentType.CENTER,
							})
						],
						borders: {
							top: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							bottom: {
								style: BorderStyle.SINGLE,
								size: 1,
								color: "black",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(55),
							type: WidthType.DXA,
						},
					}),
					new TableCell({
						//третья клетка третьего ряда
						children: [],
						borders: {
							top: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							bottom: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(5),
							type: WidthType.DXA,
						},
					}),
					new TableCell({
						//четвёртая клетка третьего ряда
						children: [],
						borders: {
							top: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							bottom: {
								style: BorderStyle.SINGLE,
								size: 1,
								color: "black",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(30),
							type: WidthType.DXA,
						},
					}),
					new TableCell({
						//пятая клетка третьего ряда
						children: [],
						borders: {
							top: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							bottom: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(5),
							type: WidthType.DXA,
						},
					}),
					new TableCell({
						//шесстая клетка третьего ряда
						children: [
							new Paragraph({
								text: student_name,
								style: "BaseStyle",
								alignment: AlignmentType.CENTER,
							})
						],
						borders: {
							top: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							bottom: {
								style: BorderStyle.SINGLE,
								size: 1,
								color: "black",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(50),
							type: WidthType.DXA,
						},
					})
				]
			}),
			new TableRow({
				//четвёртый ряд
				children: [
					new TableCell({
						//первая клетка четвёртого ряда
						children: [],
						borders: {
							top: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							bottom: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(25),
							type: WidthType.DXA,
						},
					}),
					new TableCell({
						//вторая клетка четвёртого ряда
						children: [
							new Paragraph({
								text: "Номер группы, зачётной книжки",
								style: "BaseTinyStyle",
								alignment: AlignmentType.CENTER,
								verticalAlign: VerticalAlign.TOP,
							}),
						],
						borders: {
							top: {
								style: BorderStyle.SINGLE,
								size: 1,
								color: "black",
							},
							bottom: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(55),
							type: WidthType.DXA,
						},
					}),
					new TableCell({
						//третья клетка четвёртого ряда
						children: [],
						borders: {
							top: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							bottom: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(5),
							type: WidthType.DXA,
						},
					}),
					new TableCell({
						//четвертая клетка четвёртого ряда
						children: [
							new Paragraph({
								text: "подпись, дата",
								style: "BaseTinyStyle",
								alignment: AlignmentType.CENTER,
								verticalAlign: VerticalAlign.TOP,
							}),
						],
						borders: {
							top: {
								style: BorderStyle.SINGLE,
								size: 1,
								color: "black",
							},
							bottom: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(30),
							type: WidthType.DXA,
						},
					}),
					new TableCell({
						//пятая клетка четвёртого ряда
						children: [],
						borders: {
							top: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							bottom: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(5),
							type: WidthType.DXA,
						},
					}),
					new TableCell({
						//шестая клетка четвёртого ряда
						children: [
							new Paragraph({
								text: "инициалы, фамилия",
								style: "BaseTinyStyleStyle",
								alignment: AlignmentType.CENTER,
								verticalAlign: VerticalAlign.TOP,
							})
						],
						borders: {
							top: {
								style: BorderStyle.SINGLE,
								size: 1,
								color: "black",
							},
							bottom: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(50),
							type: WidthType.DXA,
						},
					})
				]
			}),
		]
	})

	const TitleTableTwo = new Table({
		alignment: AlignmentType.CENTER,
		rows: [
			new TableRow({
				children: [
					new TableCell({
						children: [
							new Paragraph({
								text: theme,
								style: "BaseStyle",
								alignment: AlignmentType.CENTER,
							})
						],
						borders: {
							top: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							bottom: {
								style: BorderStyle.SINGLE,
								size: 1,
								color: "black",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(170),
							type: WidthType.DXA,
						},
					}),
				],
			}),
			new TableRow({
				children: [
					new TableCell({
						children: [
							new Paragraph({
								text: "тема",
								alignment: AlignmentType.CENTER,
								verticalAlign: VerticalAlign.TOP,
								style: "BaseTinyStyle",
							})
						],
						borders: {
							top: {
								style: BorderStyle.SINGLE,
								size: 1,
								color: "black",
							},
							bottom: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(170),
							type: WidthType.DXA,
						},
					}),
				],
			}),
		],
	});

	const TitleTableOne = new Table({
		alignment: AlignmentType.CENTER,
		rows: [
			new TableRow({
				children: [
					new TableCell({
						children: [
							new Paragraph({
								text: institute,
								style: "BaseStyle",
								alignment: AlignmentType.CENTER,
							})
						],
						borders: {
							top: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							bottom: {
								style: BorderStyle.SINGLE,
								size: 1,
								color: "black",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(170),
							type: WidthType.DXA,
						},
					}),
				],
			}),
			new TableRow({
				children: [
					new TableCell({
						children: [
							new Paragraph({
								text: "институт",
								alignment: AlignmentType.CENTER,
								verticalAlign: VerticalAlign.TOP,
								style: "BaseTinyStyle",
							})
						],
						borders: {
							top: {
								style: BorderStyle.SINGLE,
								size: 1,
								color: "black",
							},
							bottom: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(170),
							type: WidthType.DXA,
						},
					}),
				],
			}),
			new TableRow({
				children: [
					new TableCell({
						children: [
							new Paragraph({
								text: department,
								style: "BaseStyle",
								alignment: AlignmentType.CENTER,
							})
						],
						borders: {
							top: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							bottom: {
								style: BorderStyle.SINGLE,
								size: 1,
								color: "black",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(170),
							type: WidthType.DXA,
						},
					}),
				],
			}),
			new TableRow({
				children: [
					new TableCell({
						children: [
							new Paragraph({
								text: "кафедра",
								alignment: AlignmentType.CENTER,
								verticalAlign: VerticalAlign.TOP,
								style: "BaseTinyStyle",
							})
						],
						borders: {
							top: {
								style: BorderStyle.SINGLE,
								size: 1,
								color: "black",
							},
							bottom: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							left: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
							right: {
								style: BorderStyle.NONE,
								size: 1,
								color: "white",
							},
						},
						width: {
							size: convertMillimetersToTwip(170),
							type: WidthType.DXA,
						},
					}),
				],
			}),
		],
	});

	
	return {
		properties: {
            page: {
                margin: {
                    top: convertMillimetersToTwip(20),
                    right: convertMillimetersToTwip(10),
                    bottom: convertMillimetersToTwip(20),
                    left: convertMillimetersToTwip(30),
                },
            },
        },
		children: title_1().concat([
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			TitleTableOne,
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			new Paragraph({
				alignment: AlignmentType.CENTER,
				style: "BaseStyle",
				children:[ 
					new TextRun({
						bold: true,
						text: "ОТЧЕТ О ПРАКТИЧЕСКОЙ РАБОТЕ",
					}),
				],
			}),
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			TitleTableTwo,
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			TitleTableThree,
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			new Paragraph({text: "", style: "BaseStyle", alignment: AlignmentType.CENTER,}),
			new Paragraph({
				text: "Красноярск 2021",
				style: "BaseStyle",
				alignment: AlignmentType.CENTER,
			}),
		]),
	}
};

/*const doc = new Document({
		styles:
		{
			paragraphStyles: [
				{
					id: "BaseStyle",
					name: "Base style",
					basedOn: "WithoutIntervaks",
					next: "NoIdentation",
					quickFormat: true,
					run: {
						font: "Times New Roman",
						size: 14*2,
					},
					paragraph: {
						spacing: { line: 276, before: 0, after: 0 },
						rightTabStop: 0,
						leftTabStop: 0,
					},
				},
				{
					id: "BaseTinyStyle",
					name: "Base Tiny",
					basedOn: "WithoutIntervaks",
					next: "NoIdentation",
					quickFormat: true,
					run: {
						font: "Times New Roman",
						size: 10*2,
					},
					paragraph: {
						spacing: { line: 276, before: 0, after: 0 },
						rightTabStop: 0,
						leftTabStop: 0,
					},
				},
			],
		}
		
	})*/
