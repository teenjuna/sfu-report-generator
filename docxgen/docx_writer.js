import * as fs from "fs";
import {
  Document,
  BorderStyle,
  Packer,
  Paragraph,
  TextRun,
  convertMillimetersToTwip,
  AlignmentType,
  Footer,
  Media,
  Table,
  TableCell,
  TableRow,
  WidthType,
  VerticalAlign,
  PageNumber,
  ImageRun,
  InternalHyperlink,
  Bookmark,
} from "docx";

import fetch from 'node-fetch';

import {
  title_practic_work
} from "./title_pages/title_page_practice_work.js";

// functions, that generates structural elements
function create_essay(essay) {
  let paragraphs = [
    new Paragraph({
      children: [new TextRun({ text: "РЕФЕРАТ", bold: true })],
      alignment: AlignmentType.CENTER,
      style: "BaseStyle",
      spacing: {
        before: 0,
        after: 280,
        line: 340,
        lineRule: "exact",
      },
    }),
  ];
  for (let i = 0; i < essay.length; i++) {
    let element = essay[i];
    if (element.type == "text") {
      let s = "";
      for (let j = 0; j < element.text.length; j++) {
        for (let k = 0; k < element.text[j].length; k++) {
          if (k == element.text[j].length - 1) s += element.text[j][k] + " ";
          else s += element.text[j][k] + "\ufeff \ufeff";
        }
      }
      paragraphs.push(
        new Paragraph({
          text: s,
          alignment: AlignmentType.JUSTIFIED,
          style: "BaseStyle",
          spacing: {
            before: Math.floor(element.sb / 50),
            line: 340,
            lineRule: "exact",
          },
        })
      );
    }
    if (element.type == "error") {
      let s = "";
      for (let j = 0; j < element.text.length; j++) {
        for (let k = 0; k < element.text[j].length; k++) {
          if (k == element.text[j].length - 1) s += element.text[j][k] + " ";
          else s += element.text[j][k] + "\ufeff \ufeff";
        }
      }
      paragraphs.push(
        new Paragraph({
          children: [
            new TextRun({
              text: s,
              color: "ff0000",
              font: "Times New Roman",
              size: 24 * 2,
            }),
          ],
          alignment: AlignmentType.CENTER,
          spacing: {
            before: Math.floor(element.sb / 50),
            after: 0,
            line: 480,
            lineRule: "exact",
          },
        })
      );
    }
  }
  return {
    margins: {
      top: convertMillimetersToTwip(20),
      left: convertMillimetersToTwip(30),
      right: convertMillimetersToTwip(10),
      bottom: convertMillimetersToTwip(20),
    },
    children: paragraphs,
    footers: {
      default: new Footer({
        children: [
          new Paragraph({
            children: [new TextRun({ children: [PageNumber.CURRENT] })],
            alignment: AlignmentType.CENTER,
            style: "BaseStyle",
          }),
        ],
      }),
    },
  };
}

function create_table_of_contents(toc) {
  let toc_pars = [
    new Paragraph({
      children: [new TextRun({ text: "СОДЕРЖАНИЕ", bold: true })],
      alignment: AlignmentType.CENTER,
      style: "BaseStyle",
      spacing: {
        before: 0,
        after: 280,
        line: 340,
        lineRule: "exact",
      },
    }),
  ];
  for (let i = 0; i < toc.paragraphs.length; i++) {
    for (let j = 0; j < toc.paragraphs[i].text.length; j++) {
      toc_pars.push(
        new Paragraph({
          children: [
            new InternalHyperlink({
              child: new TextRun({text: toc.paragraphs[i].text[j]}),
              anchor: "heading" + i,
              tooltip: "Подсказка"
            })
          ],
          pageBreakBefore: toc.paragraphs[i].req_pb && j == 0,
          style: "BaseStyle",
          alignment: AlignmentType.LEFT,
          spacing: {
            before: 0,
            after: 0,
            line: 340,
            lineRule: "exact",
          },
          rightTabStop: 9637,
        })
      );
    }
  }
  return {
    properties: {
            page: {
                margin: {
                    top: convertMillimetersToTwip(20),
                    right: convertMillimetersToTwip(10),
                    bottom: convertMillimetersToTwip(20),
                    left: convertMillimetersToTwip(30),
                },
            },
        },
    children: toc_pars,
    footers: {
      default: new Footer({
        children: [
          new Paragraph({
            children: [new TextRun({ children: [PageNumber.CURRENT] })],
            alignment: AlignmentType.CENTER,
            style: "BaseStyle",
          }),
        ],
      }),
    },
  };
}

function create_introduction(introduction) {
  let paragraphs = [
    new Paragraph({
      children: [new TextRun({ text: "ВВЕДЕНИЕ", bold: true })],
      alignment: AlignmentType.CENTER,
      style: "BaseStyle",
      spacing: {
        before: 0,
        after: 280,
        line: 340,
        lineRule: "exact",
      },
    }),
  ];
  for (let i = 0; i < introduction.length; i++) {
    let element = introduction[i];
    if (element.type == "text") {
      let s = "";
      for (let j = 0; j < element.text.length; j++) {
        for (let k = 0; k < element.text[j].length; k++) {
          if (k == element.text[j].length - 1) s += element.text[j][k] + " ";
          else s += element.text[j][k] + "\ufeff \ufeff";
        }
      }
      paragraphs.push(
        new Paragraph({
          text: s,
          alignment: AlignmentType.JUSTIFIED,
          style: "BaseStyle",
          spacing: {
            before: Math.floor(element.sb / 50),
            line: 340,
            lineRule: "exact",
          },
        })
      );
    }
    if (element.type == "error") {
      let s = "";
      for (let j = 0; j < element.text.length; j++) {
        for (let k = 0; k < element.text[j].length; k++) {
          if (k == element.text[j].length - 1) s += element.text[j][k] + " ";
          else s += element.text[j][k] + "\ufeff \ufeff";
        }
      }
      paragraphs.push(
        new Paragraph({
          children: [
            new TextRun({
              text: s,
              color: "ff0000",
              font: "Times New Roman",
              size: 24 * 2,
            }),
          ],
          alignment: AlignmentType.CENTER,
          spacing: {
            before: Math.floor(element.sb / 50),
            after: 0,
            line: 480,
            lineRule: "exact",
          },
        })
      );
    }
  }
  return {
    margins: {
      top: convertMillimetersToTwip(20),
      left: convertMillimetersToTwip(30),
      right: convertMillimetersToTwip(10),
      bottom: convertMillimetersToTwip(20),
    },
    children: paragraphs,
    footers: {
      default: new Footer({
        children: [
          new Paragraph({
            children: [new TextRun({ children: [PageNumber.CURRENT] })],
            alignment: AlignmentType.CENTER,
            style: "BaseStyle",
          }),
        ],
      }),
    },
  };
}

function create_main_part(main_part) {
  let paragraphs = parse_pages_(main_part.pages);
  return {
    children: paragraphs,
     properties: {
            page: {
                margin: {
                    top: convertMillimetersToTwip(20),
                    right: convertMillimetersToTwip(10),
                    bottom: convertMillimetersToTwip(20),
                    left: convertMillimetersToTwip(30),
                },
            },
        },
    footers: {
      default: new Footer({
        children: [
          new Paragraph({
            children: [new TextRun({ children: [PageNumber.CURRENT] })],
            alignment: AlignmentType.CENTER,
            style: "BaseStyle",
          }),
        ],
      }),
    },
  };
}

function create_conclusion(conclusion) {
  let paragraphs = [
    new Paragraph({
      children: [new TextRun({ text: "ЗАКЛЮЧЕНИЕ", bold: true })],
      alignment: AlignmentType.CENTER,
      style: "BaseStyle",
      spacing: {
        before: 0,
        after: 280,
        line: 340,
        lineRule: "exact",
      },
    }),
  ];
  for (let i = 0; i < structure.conclusion.length; i++) {
    let element = conclusion[i];
    if (element.type == "text") {
      let s = "";
      for (let j = 0; j < element.text.length; j++) {
        for (let k = 0; k < element.text[j].length; k++) {
          if (k == element.text[j].length - 1) s += element.text[j][k] + " ";
          else s += element.text[j][k] + "\ufeff \ufeff";
        }
      }
      paragraphs.push(
        new Paragraph({
          text: s,
          alignment: AlignmentType.JUSTIFIED,
          style: "BaseStyle",
          spacing: {
            before: Math.floor(element.sb / 50),
            line: 340,
            lineRule: "exact",
          },
        })
      );
    }
    if (element.type == "error") {
      let s = "";
      for (let j = 0; j < element.text.length; j++) {
        for (let k = 0; k < element.text[j].length; k++) {
          if (k == element.text[j].length - 1) s += element.text[j][k] + " ";
          else s += element.text[j][k] + "\ufeff \ufeff";
        }
      }
      paragraphs.push(
        new Paragraph({
          children: [
            new TextRun({
              text: s,
              color: "ff0000",
              font: "Times New Roman",
              size: 24 * 2,
            }),
          ],
          alignment: AlignmentType.CENTER,
          spacing: {
            before: Math.floor(element.sb / 50),
            after: 0,
            line: 480,
            lineRule: "exact",
          },
        })
      );
    }
  }
  return {
    margins: {
      top: convertMillimetersToTwip(20),
      left: convertMillimetersToTwip(30),
      right: convertMillimetersToTwip(10),
      bottom: convertMillimetersToTwip(20),
    },
    children: paragraphs,
    footers: {
      default: new Footer({
        children: [
          new Paragraph({
            children: [new TextRun({ children: [PageNumber.CURRENT] })],
            alignment: AlignmentType.CENTER,
            style: "BaseStyle",
          }),
        ],
      }),
    },
  };
}

function create_list_of_abbreviations(list_of_abbreviations) {
  let paragraphs = [
    new Paragraph({
      children: [new TextRun({ text: "СПИСОК СОКРАЩЕНИЙ", bold: true })],
      alignment: AlignmentType.CENTER,
      style: "BaseStyle",
      spacing: {
        before: 0,
        after: 280,
        line: 340,
        lineRule: "exact",
      },
    }),
  ];
  for (let i = 0; i < list_of_abbreviations.length; i++) {
    let element = list_of_abbreviations[i];
    if (element.type == "text") {
      let s = "";
      for (let j = 0; j < element.text.length; j++) {
        for (let k = 0; k < element.text[j].length; k++) {
          if (k == element.text[j].length - 1) s += element.text[j][k] + " ";
          else s += element.text[j][k] + "\ufeff \ufeff";
        }
      }
      paragraphs.push(
        new Paragraph({
          text: s,
          alignment: AlignmentType.JUSTIFIED,
          style: "BaseStyle",
          spacing: {
            before: Math.floor(element.sb / 50),
            line: 340,
            lineRule: "exact",
          },
        })
      );
    }
  }
  return {
    margins: {
      top: convertMillimetersToTwip(20),
      left: convertMillimetersToTwip(30),
      right: convertMillimetersToTwip(10),
      bottom: convertMillimetersToTwip(20),
    },
    children: paragraphs,
    footers: {
      default: new Footer({
        children: [
          new Paragraph({
            children: [new TextRun({ children: [PageNumber.CURRENT] })],
            alignment: AlignmentType.CENTER,
            style: "BaseStyle",
          }),
        ],
      }),
    },
  };
}

function parse_pages(pages){
  let paragraphs = [];
  for(let page_n = 0; page_n < pages.length; page_n++)
  {
    let require_pb = page_n != 0;
    for(let i = 0; i < pages[page_n].length; i++)
    {
      let element = pages[page_n][i];
      if (element.type == "text") {
      require_pb = false;
      let s = "";
      if(element.require_tab)
        s += '\t'
      for (let j = 0; j < element.text.length; j++) {
        for (let k = 0; k < element.text[j].length; k++) {
          if (k == element.text[j].length - 1) s += element.text[j][k] + " ";
          else s += element.text[j][k] + "\ufeff \ufeff";
        }
      }
      paragraphs.push(
        new Paragraph({
          text: s,
          alignment: AlignmentType.JUSTIFIED,
          style: "BaseStyle",
          spacing: {
            before: Math.floor(element.sb / 50),
            line: 340,
            lineRule: "exact",
            },
          })
        );
      }
      if (element.type == "chapter_heading") {
      let s = "\t";
      for (let j = 0; j < element.text.length; j++) {
        for (let k = 0; k < element.text[j].length; k++) {
          if (k == element.text[j].length - 1) s += element.text[j][k] + " ";
          else s += element.text[j][k] + "\ufeff \ufeff";
        }
      }
      paragraphs.push(
        new Paragraph({
          children: [new TextRun({ text: s, bold: true })],
          alignment: AlignmentType.JUSTIFIED,
          style: "BaseStyle",
          spacing: {
            line: 340,
            lineRule: "exact",
            before: Math.floor(element.sb / 50),
            after: 0,
          },
          pageBreakBefore: require_pb,
        })
      );
      require_pb = false;
    }
      if (element.type == "pre_table_text") {
      let s = "";
      for (let j = 0; j < element.text.length; j++) {
        for (let k = 0; k < element.text[j].length; k++) {
          if (k == element.text[j].length - 1) s += element.text[j][k] + " ";
          else s += element.text[j][k] + "\ufeff \ufeff";
        }
      }
      paragraphs.push(
        new Paragraph({
          text: s,
          alignment: AlignmentType.JUSTIFIED,
          style: "BaseStyle",
          spacing: {
            line: 340,
            lineRule: "exact",
            before: Math.floor(element.sb / 50),
            after: 120,
          },
          pageBreakBefore: require_pb,
        })
      );
      require_pb = false;
    }
      if (element.type == "table") {
      let rows = [];
      for (let i = 0; i < element.data.length; i++) {
        let row = [];
        for (let j = 0; j < element.data[i].length; j++) {
          //
          let s = "";
          for (let k = 0; k < element.data[i][j].length; k++) {
            for (let o = 0; o < element.data[i][j][k].length; o++) {
              if (o == element.data[i][j][k].length - 1)
                s += element.data[i][j][k][o] + " ";
              else s += element.data[i][j][k][o] + "\ufeff \ufeff";
            }
          }

          if (i == 0) {
            row.push(
              new TableCell({
                children: [
                  new Paragraph({
                    text: s,
                    spacing: {
                      line: 280,
                      lineRule: "exact",
                    },
                    style: "BaseTable",
                    alignment: AlignmentType.CENTER,
                  }),
                ],
                width: {
                  size: convertMillimetersToTwip(element.sizes[j]),
                  type: WidthType.DXA,
                },
                verticalAlign: VerticalAlign.CENTER,
                borders: {
                  top: {
                    style: BorderStyle.SINGLE,
                    size: 3,
                    color: "black",
                  },
                  bottom: {
                    style: BorderStyle.DOUBLE,
                    size: 3,
                    color: "black",
                  },
                  left: {
                    style: BorderStyle.SINGLE,
                    size: 3,
                    color: "black",
                  },
                  right: {
                    style: BorderStyle.SINGLE,
                    size: 3,
                    color: "black",
                  },
                },
              })
            );
          } else {
            row.push(
              new TableCell({
                children: [
                  new Paragraph({
                    text: s,
                    spacing: {
                      line: 280,
                      lineRule: "exact",
                    },
                    style: "BaseTable",
                    alignment: AlignmentType.CENTER,
                  }),
                ],
                width: {
                  size: convertMillimetersToTwip(element.sizes[j]),
                  type: WidthType.DXA,
                },
                verticalAlign: VerticalAlign.CENTER,
                borders: {
                  top: {
                    style: BorderStyle.SINGLE,
                    size: 3,
                    color: "black",
                  },
                  bottom: {
                    style: BorderStyle.SINGLE,
                    size: 3,
                    color: "black",
                  },
                  left: {
                    style: BorderStyle.SINGLE,
                    size: 3,
                    color: "black",
                  },
                  right: {
                    style: BorderStyle.SINGLE,
                    size: 3,
                    color: "black",
                  },
                },
              })
            );
          }
        }
        rows.push(new TableRow({ children: row }));
      }
      paragraphs.push(
        new Table({
          rows: rows,
          alignment: AlignmentType.CENTER,
        })
      );
    }
      if (element.type == "picture") {
      paragraphs.push(
        new Paragraph({
          spacing: {
            before: Math.floor(element.sb / 50),
            after: 120,
          },
          alignment: AlignmentType.CENTER,
          children: [
            Media.addImage(
              doc,
              fs.readFileSync(element.data),
              element.size[0],
              element.size[1]
            ),
          ],
          pageBreakBefore: require_pb,
        })
      );
      require_pb = false;
      let s = "";
      for (let j = 0; j < element.description.length; j++) {
        for (let k = 0; k < element.description[j].length; k++) {
          if (k == element.description[j].length - 1)
            s += element.description[j][k] + " ";
          else s += element.description[j][k] + "\ufeff \ufeff";
        }
      }
      paragraphs.push(
        new Paragraph({
          text: s,
          style: "BaseStyle",
          spacing: {
            before: 0,
            after: 0,
            line: 340,
            lineRule: "exact",
          },
          alignment: AlignmentType.CENTER,
        })
      );
    }
      if (element.type == "listing") {
      let runs = [];
      for (let i = 0; i < element.data.length; i++) {
        runs.push(new TextRun({ text: element.data[i], break: i ? 1 : 0 }));
      }
      paragraphs.push(
        new Paragraph({
          children: runs,
          style: "Listing",
        })
      );
    }
      if (element.type == "formula") {
        console.log(Buffer.from(element.data));
        paragraphs.push(
          new Paragraph({
            spacing: {
              before: Math.floor(element.sb / 50),
              after: 0,
            },
            alignment: AlignmentType.LEFT,
            children: [
              new TextRun("\t"),
              Media.addImage(
                doc,
                Buffer.from(element.data),
                element.width,
                element.height
              ),
            ],
            pageBreakBefore: requre_pb,
          })
        );
        requre_pb = false;
    }
    }
  }
  return paragraphs;
}

function placeholder()
{
  return Buffer.from([137, 80, 78, 71, 13, 10, 26, 10, 0, 0, 0, 13, 73, 72, 68, 82, 0, 0, 0, 1, 0, 0, 0, 1, 8, 2, 0, 0,
                      0, 144, 119, 83, 222, 0, 0, 0, 1, 115, 82, 71, 66, 0, 174, 206, 28, 233, 0, 0, 0, 4, 103, 65, 77,
                      65, 0, 0, 177, 143, 11, 252, 97, 5, 0, 0, 0, 9, 112, 72, 89, 115, 0, 0, 18, 116, 0, 0, 18, 116, 1,
                      222, 102, 31, 120, 0, 0, 0, 12, 73, 68, 65, 84, 24, 87, 99, 56, 124, 248, 48, 0, 4, 150, 2, 74,
                      196, 195, 6, 198, 0, 0, 0, 0, 73, 69, 78, 68, 174, 66, 96, 130]);
}

async function download(uri)
{
  const response = await fetch(uri);
  const buffer = await response.buffer();
  return buffer;
}

function parse_pages_(pages){
  let paragraphs = [];
  let heading_index = 0;
  for(let page_n = 0; page_n < pages.length; page_n++)
  {
    let require_pb = page_n != 0;
    for(let i = 0; i < pages[page_n].length; i++)
    {
      let element = pages[page_n][i];
      if (element.type == "text" || element.type == "enumeration") {
      let runs = [];
      require_pb = false;
      let s = "";
      if(element.require_tab)
        s += '\t'
      for(let j = 0; j < element.contents.length; j++)
      {
        if(j == 0)
          runs.push(new TextRun({text: s + element.contents[j], break: 0}));
        else
          runs.push(new TextRun({text: element.contents[j], break: 1}));
      }
      paragraphs.push(
        new Paragraph({
          children: runs,
          alignment: AlignmentType.JUSTIFIED,
          style: "BaseStyle",
          spacing: {
            before: Math.floor(element.sb / 50),
            line: 340,
            lineRule: "exact",
            },
          })
        );
      }
      if (element.type == "heading") {
      let runs = [];
      element.title[0] = '\t' + element.title[0]
      for (let j = 0; j < element.title.length; j++) {
        runs.push(new TextRun({text: element.title[j], break: j != 0, bold: true}))
      }
      paragraphs.push(
        new Paragraph({
          children: [new Bookmark({children: runs, id: "heading" + heading_index})],
          alignment: AlignmentType.JUSTIFIED,
          style: "BaseStyle",
          spacing: {
            line: 340,
            lineRule: "exact",
            before: Math.floor(element.sb / 50),
            after: 0,
          },
          pageBreakBefore: require_pb,
        })
      );
      require_pb = false;
      heading_index += 1;
    }
      if (element.type == "table") {
      let rows = [];
      let runs = [];
      for(let j = 0; j < element.caption.length; j++)
      {
        runs.push(new TextRun({text: element.caption[j], break: j != 0}));
      }
      paragraphs.push(
        new Paragraph({
          children: runs,
          alignment: AlignmentType.JUSTIFIED,
          style: "BaseStyle",
          spacing: {
            line: 340,
            lineRule: "exact",
            before: Math.floor(element.sb / 50),
            after: 120,
          },
          pageBreakBefore: require_pb,
        })
      );
      require_pb = false;
      for (let i = 0; i < element.rows.length; i++) {
        let row = [];
        for (let j = 0; j < element.rows[i].length; j++) {
          //
          let rns = [];
          for(let k = 0; k < element.rows[i][j].length; k++)
          {
            rns.push(new TextRun({text: element.rows[i][j][k], break: k != 0}));
          }
          if (i == 0) {
            row.push(
              new TableCell({
                children: [
                  new Paragraph({
                    children: rns,
                    spacing: {
                      line: 280,
                      lineRule: "exact",
                    },
                    style: "BaseTable",
                    alignment: AlignmentType.CENTER,
                  }),
                ],
                width: {
                  size: convertMillimetersToTwip(element.width_millimeters[j]),
                  type: WidthType.DXA,
                },
                verticalAlign: VerticalAlign.CENTER,
                borders: {
                  top: {
                    style: BorderStyle.SINGLE,
                    size: 3,
                    color: "black",
                  },
                  bottom: {
                    style: BorderStyle.DOUBLE,
                    size: 3,
                    color: "black",
                  },
                  left: {
                    style: BorderStyle.SINGLE,
                    size: 3,
                    color: "black",
                  },
                  right: {
                    style: BorderStyle.SINGLE,
                    size: 3,
                    color: "black",
                  },
                },
              })
            );
          } else {
            row.push(
              new TableCell({
                children: [
                  new Paragraph({
                    children: rns,
                    spacing: {
                      line: 280,
                      lineRule: "exact",
                    },
                    style: "BaseTable",
                    alignment: AlignmentType.CENTER,
                  }),
                ],
                width: {
                  size: convertMillimetersToTwip(element.width_millimeters[j]),
                  type: WidthType.DXA,
                },
                verticalAlign: VerticalAlign.CENTER,
                borders: {
                  top: {
                    style: BorderStyle.SINGLE,
                    size: 3,
                    color: "black",
                  },
                  bottom: {
                    style: BorderStyle.SINGLE,
                    size: 3,
                    color: "black",
                  },
                  left: {
                    style: BorderStyle.SINGLE,
                    size: 3,
                    color: "black",
                  },
                  right: {
                    style: BorderStyle.SINGLE,
                    size: 3,
                    color: "black",
                  },
                },
              })
            );
          }
        }
        rows.push(new TableRow({ children: row }));
      }
      paragraphs.push(
        new Table({
          rows: rows,
          alignment: AlignmentType.CENTER,
        })
      );
    }
      if (element.type == "image") {
      let img = download(element.uri);
      paragraphs.push(
        new Paragraph({
          spacing: {
            before: Math.floor(element.sb / 50),
            after: 120,
          },
          alignment: AlignmentType.CENTER,
          children: [
            new ImageRun({
              data: img,
              transformation: {width: element.size[0], height: element.size[1]}
            })
          ],
          pageBreakBefore: require_pb,
        })
      );
      require_pb = false;
      let runs = [];
      for (let j = 0; j < element.caption.length; j++) {
        runs.push(new TextRun({text: element.caption[j], break: j != 0}))
      }
      paragraphs.push(
        new Paragraph({
          children: runs,
          style: "BaseStyle",
          spacing: {
            before: 0,
            after: 0,
            line: 340,
            lineRule: "exact",
          },
          alignment: AlignmentType.CENTER,
        })
      );
    }
      if (element.type == "snippet") {
      let runs = [];
      for(let j = 0; j < element.caption.length; j++)
      {
        runs.push(new TextRun({text: element.caption[j], break: j != 0}))
      }
      paragraphs.push(
        new Paragraph({
          children: runs,
          alignment: AlignmentType.JUSTIFIED,
          style: "BaseStyle",
          spacing: {
            line: 340,
            lineRule: "exact",
            before: Math.floor(element.sb / 50),
            after: 120,
          },
          pageBreakBefore: require_pb,
        })
      );
      require_pb = false;
      runs = [];
      for (let j = 0; j < element.contents.length; j++) {
        runs.push(new TextRun({ text: element.contents[j], break: j ? 1 : 0 }));
      }
      paragraphs.push(
        new Paragraph({
          children: runs,
          style: "Listing",
        })
      );
    }
      if (element.type == "formula") {
        console.log(Buffer.from(element.data));
        paragraphs.push(
          new Paragraph({
            spacing: {
              before: Math.floor(element.sb / 50),
              after: 0,
            },
            alignment: AlignmentType.LEFT,
            children: [
              new TextRun("\t"),
              Media.addImage(
                doc,
                Buffer.from(element.data),
                element.width,
                element.height
              ),
            ],
            pageBreakBefore: requre_pb,
          })
        );
        requre_pb = false;
    }
    }
  }
  return paragraphs;
}

function create_document() {
  // Read arguments.
  let json_file = process.argv[2];
  let docx_file = process.argv[3];

  // Read JSON from the json_file.
  let structure = JSON.parse(fs.readFileSync(json_file, "utf8"));

  let sections = [];

  // loading the build result from a file
  // assemble other structural elements
  if(structure.info != undefined)
  {
    let info = structure.info;
    sections.push(title_practic_work(info.institute, info.department, info.theme, info.group_number, info.record_book_number, info.student_name, info.teacher_name));
  }

  if (structure.essay != undefined) {
    sections.push(create_essay(structure.essay));
  }

  if (structure.table_of_contents != undefined) {
    sections.push(create_table_of_contents(structure.table_of_contents));
  }

  if (structure.introduction != undefined) {
    sections.push(create_introduction(structure.introduction));
  }

  sections.push(create_main_part(structure.main_part));

  if (structure.conclusion != undefined) {
    sections.push(create_conclusion(structure.conclusion));
  }

  if (structure.list_of_abbreviations != undefined) {
    sections.push(
      create_list_of_abbreviations(structure.list_of_abbreviations)
    );
  }

    const doc = new Document({
    styles: {
      paragraphStyles: [
        {
          id: "BaseStyle",
          name: "Base style",
          basedOn: "Normal",
          next: "NoIdentation",
          quickFormat: true,
          run: {
            font: "Times New Roman",
            size: 14 * 2,
          },
          paragraph: {
            spacing: { line: 276, before: 0, after: 0 },
            rightTabStop: 0,
            leftTabStop: 0,
            alignment: AlignmentType.JUSTIFIED,
          },
        },
        {
          id: "BaseTinyStyle",
          name: "Base Tiny",
          basedOn: "WithoutIntervaks",
          next: "NoIdentation",
          quickFormat: true,
          run: {
            font: "Times New Roman",
            size: 10 * 2,
          },
          paragraph: {
            spacing: { line: 276, before: 0, after: 0 },
            rightTabStop: 0,
            leftTabStop: 0,
          },
        },
        {
          id: "BaseTable",
          name: "BaseStyle",
          basedOn: "WithoutIntervaks",
          next: "NoIdentation",
          quickFormat: true,
          run: {
            font: "Times New Roman",
            size: 12 * 2,
          },
          paragraph: {
            spacing: { line: 276, before: 0, after: 0 },
            rightTabStop: 0,
            leftTabStop: 0,
          },
        },
        {
          id: "Listing",
          name: "Listing",
          basedOn: "Normal",
          next: "NoIdentation",
          quickFormat: true,
          run: {
            font: "Courier New",
            size: 10 * 2,
          },
          paragraph: {
            spacing: { line: 240, before: 0, after: 0, lineRule: "exact" },
            rightTabStop: 0,
            leftTabStop: 0,
            alignment: AlignmentType.LEFT,
          },
        },
      ],
    },
    sections: sections
  });


  // output document
  Packer.toBuffer(doc).then((buffer) => {
    fs.writeFileSync(docx_file, buffer);
  });
}

create_document();
