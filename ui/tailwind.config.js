const colors = require('tailwindcss/colors');

module.exports = {
  mode: 'jit',
  purge: ['src/**/*.tsx', 'public/index.html'],
  dark: false,
  theme: {
    extend: {
      colors: {
        gray: colors.trueGray,
      },
    },
  },
  variants: {},
  plugins: [require('@tailwindcss/forms')],
};
