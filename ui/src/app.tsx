import type { Component } from 'solid-js';
import type { Report } from './types/report';
import { ReportProvider } from './contexts/report';
import { Page } from './page';
import { SegmentType } from './types/segments';
import { SelectedSegmentProvider } from './contexts/selected';
import { Paragraph } from '@/types/elements';

export const App: Component = () => {
  const report: Report = {
    info: {
      theme: '',
      teacher_name: '',
      student_name: '',
      institute: '',
      department: '',
      group_number: '',
      record_book_number: '',
    },
    chapters: [
      {
        type: SegmentType.Chapter,
        title: 'Безымянный раздел',
        elements: [],
        subchapters: [],
      },
    ],
  };

  return (
    <ReportProvider report={report}>
      <SelectedSegmentProvider>
        <Page />
      </SelectedSegmentProvider>
    </ReportProvider>
  );
};
