import type { Component } from 'solid-js';

export const Card: Component<{
  class?: string;
}> = (props) => {
  return (
    <div class={`p-2 bg-white rounded-lg ${props.class || ''}`}>
      {props.children}
    </div>
  );
};
