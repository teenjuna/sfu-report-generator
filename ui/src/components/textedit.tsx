import { Component, Match, Switch, onMount, createMemo } from 'solid-js';

export const TextEdit: Component<{
  class?: string;
  value?: string;
  onInput?: (v: string) => void;
  onBlur?: () => void;
  placeholder?: string;
  multiline?: boolean;
  rows?: number;
  autofocus?: boolean;
  tabindex?: number;
}> = (props) => {
  let inputRef: HTMLInputElement;
  let textareaRef: HTMLTextAreaElement;
  onMount(() => {
    if (props.autofocus) {
      props.multiline ? textareaRef.focus() : inputRef.focus();
    }
  });

  // prettier-ignore
  const classes = createMemo(() => `w-full p-2 bg-gray-200 rounded-lg border-none focus:ring-2 focus:ring-gray-500 ${props.class || ""}`)

  return (
    <Switch>
      <Match when={!props.multiline}>
        <input
          ref={inputRef}
          type="text"
          value={props.value}
          onInput={(e) => props.onInput && props.onInput(e.currentTarget.value)}
          onBlur={props.onBlur}
          placeholder={props.placeholder}
          class={classes()}
          tabindex={props.tabindex}
        />
      </Match>
      <Match when={props.multiline}>
        <textarea
          ref={textareaRef}
          value={props.value}
          onInput={(e) => props.onInput && props.onInput(e.currentTarget.value)}
          onBlur={props.onBlur}
          placeholder={props.placeholder}
          rows={props.rows}
          class={classes()}
          tabindex={props.tabindex}
        />
      </Match>
    </Switch>
  );
};
