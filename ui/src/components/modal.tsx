import { Component, createRenderEffect, onCleanup } from 'solid-js';
import { Portal } from 'solid-js/web';
import { XIcon } from './icons/x';

export const Modal: Component<{
  close: () => void;
}> = (props) => {
  function handleKeydown(e: KeyboardEvent) {
    if (e.key == 'Escape') {
      props.close();
    }
  }

  createRenderEffect(() => {
    window.addEventListener('keydown', handleKeydown);

    // If there was an active element, remove focus from it.
    // Otherwise modal's contents won't be able to use
    // autofocus.
    (document.activeElement as HTMLElement).blur();
  });

  onCleanup(() => {
    window.removeEventListener('keydown', handleKeydown);
  });

  return (
    <Portal mount={document.getElementById('modal')}>
      <div
        onclick={props.close}
        class={
          'absolute top-0 left-0 w-screen h-screen flex justify-center bg-black bg-opacity-40'
        }
      >
        <div
          class="mt-16 p-2 w-96 h-[fit-content] max-h-[70%] overflow-auto bg-white rounded-lg"
          onclick={(e) => e.stopPropagation()}
        >
          <div class="flex flex-row justify-end">
            <button
              class="rounded-md focus:ring-2 focus:ring-gray-400 focus:outline-none"
              onclick={props.close}
              tabindex="10"
            >
              <XIcon class="w-5 h-5" />
            </button>
          </div>
          <div class="py-2">{props.children}</div>
        </div>
      </div>
    </Portal>
  );
};
