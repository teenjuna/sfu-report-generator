import type { Component } from 'solid-js';
import { Link } from 'solid-app-router';

export const HomePage: Component = () => {
  return (
    <div class="h-full">
      <h1 class="mt-3 text-3xl font-medium">Выберите тип документа</h1>
      <div class="mt-6">
        <DocumentButton title="Отчёт" href="/editor" />
      </div>
    </div>
  );
};

const DocumentButton: Component<{
  title: string;
  href: string;
}> = (props) => {
  return (
    <Link href="/editor">
      <div class="h-48 w-36 flex justify-center items-center bg-white rounded-sm shadow-lg hover:shadow-md duration-200">
        <p class="uppercase font-medium">{props.title}</p>
      </div>
    </Link>
  );
};
