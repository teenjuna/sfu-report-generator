import {
  Component,
  createMemo,
  createSignal,
  JSX,
  onMount,
  Show,
  useContext,
} from 'solid-js';
import { Minimap } from './minimap';
import { ReportContext } from '@/contexts/report';
import { SelectedSegmentContext } from '@/contexts/selected';
import { SegmentEditor } from './segment';
import { Preview } from '@/pages/editor/preview';
import { Button } from '@/components';
import { ArrowLeft } from '@/components/icons';
import { Link } from 'solid-app-router';
import { ReportInfoFormModal } from './modals';

export const EditorPage: Component = () => {
  const [report] = useContext(ReportContext);
  const [_, { selectChapter }] = useContext(SelectedSegmentContext);

  onMount(() => {
    if (report.chapters.length > 0) {
      selectChapter({ chapter: 0 });
    }
  });

  return (
    <div class="h-full grid grid-rows-[2.5rem,auto] gap-3">
      <div class="col-span-full">
        <ButtonsPanel />
      </div>
      <div class="min-h-0 max-h-full grid grid-cols-[3fr,4fr,5fr] gap-6">
        <div class="min-w-0 min-h-0 max-h-full overflow-y-auto">
          <Minimap />
        </div>
        <div class="min-w-0 min-h-0 max-h-full overflow-y-auto">
          <SegmentEditor />
        </div>
        <div class="min-w-0 min-h-full max-h-full">
          <Preview />
        </div>
      </div>
    </div>
  );
};

const ButtonsPanel: Component = () => {
  const [showInfoModal, setShowInfoModal] = createSignal(false);
  const downloadUrl = createMemo(() => {
    const [report] = useContext(ReportContext);
    const api = import.meta.env.VITE_API_URI;
    const data = btoa(unescape(encodeURIComponent(JSON.stringify(report))));
    return `${api}/generate-docx?data=${data}`;
  });

  return (
    <>
      <div class="h-full flex justify-between">
        <div class="h-full flex space-x-4">
          <Link href="/">
            <PanelButton class="w-10">
              <ArrowLeft class="w-8 h-8" />
            </PanelButton>
          </Link>
          <PanelButton onClick={() => setShowInfoModal(true)}>
            Титульный лист
          </PanelButton>
        </div>
        <div>
          <Link external download href={downloadUrl()}>
            <PanelButton>Скачать .docx</PanelButton>
          </Link>
        </div>
      </div>
      <Show when={showInfoModal()}>
        <ReportInfoFormModal close={() => setShowInfoModal(false)} />
      </Show>
    </>
  );
};

const PanelButton: Component<JSX.ButtonHTMLAttributes<HTMLButtonElement>> = (
  props,
) => {
  return (
    <Button
      {...props}
      class={`h-full text-black bg-white flex justify-center items-center hover:ring-0 active:ring-0 focus:ring-0 hover:bg-gray-100 focus:bg-gray-100 active:bg-gray-100 ${
        props.class || ''
      }`}
    />
  );
};
