import { Component, createEffect, For, useContext } from 'solid-js';
import { Card } from '@/components/card';
import * as segments from '@/types/segments';
import { SelectedSegmentContext } from '@/contexts/selected';
import { ReportContext } from '@/contexts/report';

export const Minimap: Component = () => {
  const [report] = useContext(ReportContext);

  return (
    <div class="flex flex-col items-end">
      <Card class="min-h-[16rem] w-full rounded-br-none">
        <For each={report.chapters}>
          {(chapter, i) => (
            <Chapter chapter={{ ...chapter, path: { chapter: i() } }} />
          )}
        </For>
      </Card>
      <Card class="w-[fit-content] p-0 rounded-t-none">
        <AddChapterButton />
      </Card>
    </div>
  );
};

const Chapter: Component<{
  chapter: segments.ChapterWithPath;
}> = (props) => {
  const [_, { isChapter, selectChapter }] = useContext(SelectedSegmentContext);
  return (
    <>
      <TitleButton
        active={isChapter(props.chapter.path)}
        onclick={() => selectChapter(props.chapter.path)}
      >
        {segments.hierarchyTitle(props.chapter)}
      </TitleButton>
      <div class="pl-4">
        <For each={props.chapter.subchapters}>
          {(subchapter, i) => (
            <Subchapter
              subchapter={{
                ...subchapter,
                path: {
                  ...props.chapter.path,
                  subchapter: i(),
                },
              }}
            />
          )}
        </For>
      </div>
    </>
  );
};

const Subchapter: Component<{
  subchapter: segments.SubchapterWithPath;
}> = (props) => {
  const [_, { isSubchapter, selectSubchapter }] = useContext(
    SelectedSegmentContext,
  );
  return (
    <>
      <TitleButton
        active={isSubchapter(props.subchapter.path)}
        onclick={() => selectSubchapter(props.subchapter.path)}
      >
        {segments.hierarchyTitle(props.subchapter)}
      </TitleButton>
      <div class="pl-4">
        <For each={props.subchapter.sections}>
          {(section, i) => (
            <Section
              section={{
                ...section,
                path: {
                  ...props.subchapter.path,
                  section: i(),
                },
              }}
            />
          )}
        </For>
      </div>
    </>
  );
};

const Section: Component<{
  section: segments.SectionWithPath;
}> = (props) => {
  const [_, { isSection, selectSection }] = useContext(SelectedSegmentContext);
  return (
    <>
      <TitleButton
        active={isSection(props.section.path)}
        onclick={() => selectSection(props.section.path)}
      >
        {segments.hierarchyTitle(props.section)}
      </TitleButton>
      <div class="pl-4">
        <For each={props.section.subsections}>
          {(subsection, i) => (
            <Subsection
              subsection={{
                ...subsection,
                path: {
                  ...props.section.path,
                  subsection: i(),
                },
              }}
            />
          )}
        </For>
      </div>
    </>
  );
};

const Subsection: Component<{
  subsection: segments.SubsectionWithPath;
}> = (props) => {
  const [_, { isSubsection, selectSubsection }] = useContext(
    SelectedSegmentContext,
  );
  return (
    <>
      <TitleButton
        active={isSubsection(props.subsection.path)}
        onclick={() => selectSubsection(props.subsection.path)}
      >
        {segments.hierarchyTitle(props.subsection)}
      </TitleButton>
    </>
  );
};

const TitleButton: Component<{
  active: boolean;
  onclick?: () => void;
}> = (props) => {
  let ref: HTMLButtonElement;
  return (
    <button
      ref={ref}
      class="block w-full text-left overflow-ellipsis overflow-hidden whitespace-nowrap focus:ring-0 focus:font-bold"
      classList={{ 'font-bold': props.active }}
      onclick={() => {
        ref.blur();
        props.onclick?.();
      }}
    >
      {props.children}
    </button>
  );
};

const AddChapterButton: Component = () => {
  const [report, { chapters }] = useContext(ReportContext);
  const [_, { selectChapter }] = useContext(SelectedSegmentContext);
  let ref: HTMLButtonElement;

  const handleClick = () => {
    ref.blur();
    chapters.add(
      {
        type: segments.SegmentType.Chapter,
        title: 'Безымянный раздел',
        elements: [],
        subchapters: [],
      },
      null,
    );
    selectChapter({ chapter: report.chapters.length - 1 });
  };

  return (
    <button
      ref={ref}
      class="p-2 w-36 text-xs uppercase hover:font-bold hover:ring-0 focus:font-bold focus:ring-0"
      onclick={handleClick}
    >
      Добавить раздел
    </button>
  );
};
