import { Component, createSignal, For, Match, Show, Switch } from 'solid-js';
import { Card } from '@/components/card';
import { PenIcon } from '@/components/icons/pen';
import { TrashIcon } from '@/components/icons/trash';
import * as elements from '@/types/elements';
import {
  ImageModal,
  ParagraphModal,
  CodeSnippetModal,
  EnumerationModal,
} from '@/pages/editor/segment/modals';

export const SegmentElement: Component<{
  element: elements.SegmentElement;
  onUpdate: (el: elements.SegmentElement) => void;
  onDelete: () => void;
}> = (props) => {
  return (
    <Switch>
      <Match when={props.element.type == elements.SegmentElementType.Paragraph}>
        <ParagraphElement
          paragraph={props.element as elements.Paragraph}
          onUpdate={(prg) => props.onUpdate(prg)}
          onDelete={props.onDelete}
        />
      </Match>
      <Match when={props.element.type == elements.SegmentElementType.Image}>
        <ImageElement
          value={props.element as elements.Image}
          onUpdate={(image) => props.onUpdate(image)}
          onDelete={props.onDelete}
        />
      </Match>
      <Match
        when={props.element.type == elements.SegmentElementType.CodeSnippet}
      >
        <CodeSnippetElement
          value={props.element as elements.CodeSnippet}
          onUpdate={(cs) => props.onUpdate(cs)}
          onDelete={props.onDelete}
        />
      </Match>
      <Match
        when={props.element.type == elements.SegmentElementType.Enumeration}
      >
        <EnumerationElement
          value={props.element as elements.Enumeration}
          onUpdate={(enm) => props.onUpdate(enm)}
          onDelete={props.onDelete}
        />
      </Match>
    </Switch>
  );
};

const ParagraphElement: Component<{
  paragraph: elements.Paragraph;
  onUpdate: (contents: elements.Paragraph) => void;
  onDelete: () => void;
}> = (props) => {
  const [showEditModal, setShowEditModal] = createSignal(false);

  return (
    <>
      <Show when={showEditModal()}>
        <ParagraphModal
          value={props.paragraph.contents}
          onSubmit={props.onUpdate}
          close={() => setShowEditModal(false)}
        />
      </Show>
      <ElementCard
        label="Абзац"
        onClickEdit={() => setShowEditModal(true)}
        onClickDelete={props.onDelete}
      >
        <p class="leading-snug overflow-ellipsis overflow-hidden">
          {props.paragraph.contents}
        </p>
      </ElementCard>
    </>
  );
};

const ImageElement: Component<{
  value: elements.Image;
  onUpdate: (img: elements.Image) => void;
  onDelete: () => void;
}> = (props) => {
  const [showEditModal, setShowEditModal] = createSignal(false);

  return (
    <>
      <ElementCard
        label="Изображение"
        onClickEdit={() => setShowEditModal(true)}
        onClickDelete={props.onDelete}
      >
        <img src={props.value.uri} class="mt-2 mx-auto max-w-100 h-auto" />
        <p class="mt-1 text-center leading-snug overflow-ellipsis overflow-hidden">
          {props.value.caption}
        </p>
      </ElementCard>
      <Show when={showEditModal()}>
        <ImageModal
          onSubmit={props.onUpdate}
          close={() => setShowEditModal(false)}
          value={props.value}
        />
      </Show>
    </>
  );
};

const CodeSnippetElement: Component<{
  value: elements.CodeSnippet;
  onUpdate: (cs: elements.CodeSnippet) => void;
  onDelete: () => void;
}> = (props) => {
  const [showEditModal, setShowEditModal] = createSignal(false);

  return (
    <>
      <Show when={showEditModal()}>
        <CodeSnippetModal
          value={props.value}
          onSubmit={props.onUpdate}
          close={() => setShowEditModal(false)}
        />
      </Show>
      <ElementCard
        label="Исходный код"
        onClickEdit={() => setShowEditModal(true)}
        onClickDelete={props.onDelete}
      >
        <p class="px-1 text-sm leading-snug">{props.value.caption}</p>
        <pre class="mt-1 p-1 bg-gray-100 rounded font-mono whitespace-pre text-sm leading-snug overflow-auto">
          {props.value.contents}
        </pre>
      </ElementCard>
    </>
  );
};

const EnumerationElement: Component<{
  value: elements.Enumeration;
  onUpdate: (enm: elements.Enumeration) => void;
  onDelete: () => void;
}> = (props) => {
  const [showEditModal, setShowEditModal] = createSignal(false);

  return (
    <>
      <Show when={showEditModal()}>
        <EnumerationModal
          value={props.value}
          onSubmit={props.onUpdate}
          close={() => setShowEditModal(false)}
        />
      </Show>
      <ElementCard
        label="Перечисление"
        onClickEdit={() => setShowEditModal(true)}
        onClickDelete={props.onDelete}
      >
        <div class="flex flex-col space-y-1">
          <For each={props.value.contents}>
            {(item) => (
              <div class="flex space-x-2">
                <span class="mt-1 min-h-4 max-h-4 px-1 font-mono text-xs bg-gray-100 rounded">
                  —
                </span>
                <p>{item}</p>
              </div>
            )}
          </For>
        </div>
      </ElementCard>
    </>
  );
};

const ElementCard: Component<{
  label: string;
  onClickEdit: () => void;
  onClickDelete: () => void;
}> = (props) => {
  return (
    <Card class="mt-6">
      <div class="flex justify-between items-center">
        <h6 class="text-sm font-bold uppercase">{props.label}</h6>
        <div class="flex space-x-2">
          <button onclick={props.onClickEdit}>
            <PenIcon class="w-5 h-5" />
          </button>
          <button onclick={props.onClickDelete}>
            <TrashIcon class="w-5 h-5" />
          </button>
        </div>
      </div>
      <div class="mt-1">{props.children}</div>
    </Card>
  );
};
