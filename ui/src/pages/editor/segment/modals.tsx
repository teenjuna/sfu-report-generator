import { Modal } from '@/components/modal';
import { Button, TextEdit } from '@/components';
import {
  batch,
  Component,
  createSignal,
  createState,
  For,
  Index,
  Match,
  Show,
  Switch,
} from 'solid-js';
import {
  Enumeration,
  CodeSnippet,
  Image,
  Paragraph,
  SegmentElement,
  SegmentElementType,
} from '@/types/elements';

export const ChangeTitleModal: Component<{
  prevTitle: string;
  update: (title: string) => void;
  close: () => void;
}> = (props) => {
  const [title, setTitle] = createSignal('');

  return (
    <Modal close={props.close}>
      <form
        class="flex flex-col items-end"
        onsubmit={(e) => {
          e.preventDefault();
          props.update(title());
          props.close();
        }}
      >
        <TextEdit
          value={props.prevTitle}
          placeholder="Введите новое название"
          onInput={(title) => setTitle(title as string)}
          autofocus
        />
        <div class="mt-4">
          <Button type="submit">Сохранить</Button>
        </div>
      </form>
    </Modal>
  );
};

export const ConfirmModal: Component<{
  message: string;
  confirm: () => void;
  close: () => void;
}> = (props) => {
  return (
    <Modal close={props.close}>
      <p class="mt-2 text-center leading-snug">{props.message}</p>
      <div class="mt-4 flex justify-center space-x-2">
        <Button
          type="button"
          onclick={props.close}
          autofocus
          tabindex={1}
          class="w-20"
        >
          Отмена
        </Button>
        <Button type="button" onclick={props.confirm} tabindex={2} class="w-20">
          Ок
        </Button>
      </div>
    </Modal>
  );
};

export const AddChapterContentModal: Component<{
  onAddElement: (element: SegmentElement) => void;
  onAddSubchaper: () => void;
  showAddElement: boolean;
  showAddSubchapter: boolean;
  close: () => void;
}> = (props) => {
  return (
    <AddSegmentContentModal
      close={props.close}
      onAddElement={props.onAddElement}
      onAddChildSegment={props.onAddSubchaper}
      showAddElement={props.showAddElement}
      showAddChildSegment={props.showAddSubchapter}
      childSegmentTitle="Подраздел"
    />
  );
};

export const AddSubchapterContentModal: Component<{
  onAddElement: (element: SegmentElement) => void;
  onAddSection: () => void;
  showAddElement: boolean;
  showAddSection: boolean;
  close: () => void;
}> = (props) => {
  return (
    <AddSegmentContentModal
      close={props.close}
      onAddElement={props.onAddElement}
      onAddChildSegment={props.onAddSection}
      showAddElement={props.showAddElement}
      showAddChildSegment={props.showAddSection}
      childSegmentTitle="Пункт"
    />
  );
};

export const AddSectionContentModal: Component<{
  onAddElement: (element: SegmentElement) => void;
  onAddSubsection: () => void;
  showAddElement: boolean;
  showAddSubsection: boolean;
  close: () => void;
}> = (props) => {
  return (
    <AddSegmentContentModal
      close={props.close}
      onAddElement={props.onAddElement}
      onAddChildSegment={props.onAddSubsection}
      showAddElement={props.showAddElement}
      showAddChildSegment={props.showAddSubsection}
      childSegmentTitle="Подпункт"
    />
  );
};

export const AddSubsectionContentModal: Component<{
  onAddElement: (element: SegmentElement) => void;
  close: () => void;
}> = (props) => {
  return (
    <AddSegmentContentModal
      close={props.close}
      onAddElement={props.onAddElement}
      onAddChildSegment={() => {}}
      showAddElement={true}
      showAddChildSegment={false}
      childSegmentTitle=""
    />
  );
};

const AddSegmentContentModal: Component<{
  close: () => void;
  onAddElement: (element: SegmentElement) => void;
  onAddChildSegment: () => void;
  showAddElement: boolean;
  showAddChildSegment: boolean;
  childSegmentTitle: string;
}> = (props) => {
  const [pickedType, setPickedType] = createSignal<SegmentElementType>();

  return (
    <Switch>
      <Match when={!pickedType()}>
        <ElementOptionsModal
          close={props.close}
          onElementPick={(et) => setPickedType(et)}
          onChildSegmentPick={() => {
            props.close();
            props.onAddChildSegment();
          }}
          childSegmentTitle={props.childSegmentTitle}
          allowElements={props.showAddElement}
          allowChildSegment={props.showAddChildSegment}
        />
      </Match>
      <Match when={pickedType}>
        <ElementModal
          type={pickedType()}
          close={props.close}
          onSubmit={(el) => {
            props.close();
            props.onAddElement(el);
          }}
        />
      </Match>
    </Switch>
  );
};

const ElementOptionsModal: Component<{
  close: () => void;
  onElementPick: (et: SegmentElementType) => void;
  onChildSegmentPick: () => void;
  childSegmentTitle: string;
  allowElements: boolean;
  allowChildSegment: boolean;
}> = (props) => {
  const options = [
    [SegmentElementType.Paragraph, 'Абзац'],
    [SegmentElementType.Image, 'Изображение'],
    [SegmentElementType.CodeSnippet, 'Исходный код'],
    [SegmentElementType.Enumeration, 'Перечисление'],
  ];

  return (
    <Modal close={props.close}>
      <div class="grid grid-cols-2 gap-3">
        <Show when={props.allowChildSegment}>
          <Button
            onClick={() => props.onChildSegmentPick()}
            class="w-full h-12 col-span-full"
          >
            {props.childSegmentTitle}
          </Button>
        </Show>
        <Show when={props.allowElements}>
          <For each={options}>
            {(v) => (
              <Button
                onClick={() => props.onElementPick(v[0] as SegmentElementType)}
                class="w-full h-12"
              >
                {v[1]}
              </Button>
            )}
          </For>
        </Show>
      </div>
    </Modal>
  );
};

const ElementModal: Component<{
  type: SegmentElementType;
  close: () => void;
  onSubmit: (el: SegmentElement) => void;
}> = (props) => {
  return (
    <Switch>
      <Match when={props.type == SegmentElementType.Paragraph}>
        <ParagraphModal close={props.close} onSubmit={props.onSubmit} />
      </Match>
      <Match when={props.type == SegmentElementType.Image}>
        <ImageModal close={props.close} onSubmit={props.onSubmit} />
      </Match>
      <Match when={props.type == SegmentElementType.CodeSnippet}>
        <CodeSnippetModal close={props.close} onSubmit={props.onSubmit} />
      </Match>
      <Match when={props.type == SegmentElementType.Enumeration}>
        <EnumerationModal close={props.close} onSubmit={props.onSubmit} />
      </Match>
    </Switch>
  );
};

export const ParagraphModal: Component<{
  value?: string;
  close: () => void;
  onSubmit: (text: Paragraph) => void;
}> = (props) => {
  const [text, setText] = createSignal(props.value || '');

  return (
    <Modal close={props.close}>
      <TextEdit
        value={text()}
        onInput={(text) => setText(text)}
        multiline
        rows={10}
        tabindex={1}
        autofocus
      />
      <div class="mt-3 flex justify-end">
        <Button
          type="button"
          onclick={() => {
            props.onSubmit(new Paragraph(text()));
            props.close();
          }}
          tabindex={2}
          class="w-20"
        >
          Ок
        </Button>
      </div>
    </Modal>
  );
};

export const ImageModal: Component<{
  value?: Image;
  close: () => void;
  onSubmit: (img: Image) => void;
}> = (props) => {
  const [img, setImg] = createState(props.value);

  return (
    <Modal close={props.close}>
      <TextEdit
        value={img.caption}
        onInput={(caption) => setImg({ ...img, caption })}
        placeholder="Подпись"
        tabindex={1}
        autofocus
      />
      <TextEdit
        value={img.uri}
        onInput={(uri) => setImg({ ...img, uri })}
        placeholder="Ссылка на картинку"
        tabindex={2}
        class="mt-3"
      />
      <div class="mt-3 flex justify-end">
        <Button
          type="button"
          onclick={() => {
            props.onSubmit(new Image(img.uri, img.caption));
            props.close();
          }}
          tabindex={3}
          class="w-20"
        >
          Ок
        </Button>
      </div>
    </Modal>
  );
};

export const CodeSnippetModal: Component<{
  value?: CodeSnippet;
  close: () => void;
  onSubmit: (cs: CodeSnippet) => void;
}> = (props) => {
  const [cs, setCs] = createState(props.value);

  return (
    <Modal close={props.close}>
      <TextEdit
        value={cs.caption}
        onInput={(caption) => setCs({ ...cs, caption })}
        placeholder="Подпись"
        tabindex={1}
        autofocus
      />
      <TextEdit
        value={cs.contents}
        onInput={(contents) => setCs({ ...cs, contents })}
        placeholder="Исходный код"
        multiline
        rows={10}
        tabindex={2}
        class="mt-3 font-mono text-sm"
      />
      <div class="mt-3 flex justify-end">
        <Button
          type="button"
          onclick={() => {
            props.onSubmit(new CodeSnippet(cs.contents, cs.caption));
            props.close();
          }}
          tabindex={3}
          class="w-20"
        >
          Ок
        </Button>
      </div>
    </Modal>
  );
};

export const EnumerationModal: Component<{
  value?: Enumeration;
  close: () => void;
  onSubmit: (cs: Enumeration) => void;
}> = (props) => {
  const [enm, setEnm] = createState({
    items: props.value?.contents.concat(['']) || [''],
  });

  return (
    <Modal close={props.close}>
      <div class="flex flex-col space-y-3">
        <Index each={enm.items}>
          {(item, i) => (
            <TextEdit
              value={item()}
              placeholder={`#${i + 1}...`}
              onInput={(v) => {
                batch(() => {
                  setEnm('items', i, v);
                  if (i === enm.items.length - 1) setEnm('items', i + 1, '');
                });
              }}
              onBlur={() => {
                setEnm('items', (items) =>
                  items.filter((item_, i_) => {
                    if (item_.trim() == '' && i_ != enm.items.length - 1) {
                      console.log('filtering out input', i_);
                      return false;
                    }
                    return true;
                  }),
                );
              }}
              multiline
              rows={1}
              autofocus={i == 0}
            />
          )}
        </Index>
      </div>
      <div class="mt-3 flex justify-end">
        <Button
          type="button"
          onclick={() => {
            props.onSubmit(
              new Enumeration(enm.items.slice(0, enm.items.length - 1)),
            );
            props.close();
          }}
          tabindex={3}
          class="w-20"
        >
          Ок
        </Button>
      </div>
    </Modal>
  );
};
