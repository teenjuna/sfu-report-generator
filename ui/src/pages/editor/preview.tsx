import { Card } from '@/components';
import { ReportContext } from '@/contexts/report';
import {
  Component,
  createEffect,
  createSignal,
  For,
  Match,
  Switch,
  useContext,
} from 'solid-js';

export const Preview: Component = () => {
  const [report] = useContext(ReportContext);
  const [loading, setLoading] = createSignal(true);
  const [pages, setPages] = createSignal();
  createEffect(() => {
    setLoading(true);
    report.chapters; // register dependency
    const api = import.meta.env.VITE_API_URI;
    const data = btoa(unescape(encodeURIComponent(JSON.stringify(report))));
    let req = new XMLHttpRequest();
    req.open('GET', `${api}/generate?data=${data}`);
    req.send();
    req.onload = () => {
      setPages(JSON.parse(req.response).main_part.pages);
      setLoading(false);
      console.debug('[Preview] received flat data:', pages());
    };
  });

  return (
    <Card
      class={`max-h-full min-h-full overflow-auto ${
        loading() ? 'opacity-50' : ''
      }`}
    >
      <For each={pages() as any[]}>
        {(page, i) => (
          <div class="h-full">
            <Page page={page} />
            <h2 class="my-1 text-center">{`${i() + 1}`}</h2>
          </div>
        )}
      </For>
    </Card>
  );
};

// WARNING: the code below is a wild untyped shit. Beware.

const Page: Component<{ page: any }> = (props) => {
  return (
    <div class="">
      <For each={props.page}>{(element) => <Element element={element} />}</For>
    </div>
  );
};

const Element: Component<{ element: any }> = (props) => {
  return (
    <div style={{ 'margin-top': `${props.element.sb_percentage / 100}rem` }}>
      <Switch>
        <Match when={props.element.type == 'heading'}>
          <Heading text={props.element.title.join('\n')} />
        </Match>
        <Match when={['text', 'enumeration'].includes(props.element.type)}>
          <Text
            text={props.element.contents.join('\n')}
            indent={props.element.require_tab}
          />
        </Match>
        <Match when={props.element.type == 'snippet'}>
          <CodeSnippet
            caption={props.element.caption.join('\n')}
            code={props.element.contents.join('\n')}
          />
        </Match>
        <Match when={props.element.type == 'image'}>
          <Image
            caption={props.element.caption.join('\n')}
            uri={props.element.uri}
          />
        </Match>
      </Switch>
    </div>
  );
};

const Heading: Component<{ text: string }> = (props) => {
  return (
    <h3 style={{ 'text-indent': '2.5rem' }} class="font-bold">
      {props.text}
    </h3>
  );
};

const Text: Component<{ text: string; indent: boolean }> = (props) => {
  return (
    <p
      style={{ 'text-indent': props.indent ? '2.5rem' : 'none' }}
      class="text-justify"
    >
      {props.text}
    </p>
  );
};

const CodeSnippet: Component<{ caption: string; code: string }> = (props) => {
  return (
    <div>
      <Text text={props.caption} indent />
      <pre class="whitespace-pre-wrap break-all text-xs">{props.code}</pre>
    </div>
  );
};

const Image: Component<{ uri: string; caption: string }> = (props) => {
  return (
    <div>
      <img src={props.uri} alt={props.caption} class="mx-auto" />
      <p class="mt-1 text-center">{props.caption}</p>
    </div>
  );
};
