import { Button, TextEdit } from '@/components';
import { Modal } from '@/components/modal';
import { ReportContext } from '@/contexts/report';
import { Component, createState, useContext } from 'solid-js';
import type { ReportInfo } from '@/types/report';

export const ReportInfoFormModal: Component<{
  close: () => void;
}> = (props) => {
  const [report, { info: infoStorage }] = useContext(ReportContext);
  const [info, setInfo] = createState<ReportInfo>(report.info);
  const handleSubmit = () => {
    infoStorage.set(info);
    props.close();
  };

  return (
    <Modal close={props.close}>
      <div>
        <TextEdit
          onInput={(theme) => setInfo({ ...info, theme })}
          value={info.theme}
          placeholder="Тема"
          autofocus
        />
      </div>
      <div class="mt-3">
        <TextEdit
          onInput={(teacher_name) => setInfo({ ...info, teacher_name })}
          value={info.teacher_name}
          placeholder="Имя преподавателя"
        />
      </div>
      <div class="mt-3">
        <TextEdit
          onInput={(institute) => setInfo({ ...info, institute })}
          value={info.institute}
          placeholder="Институт"
        />
      </div>
      <div class="mt-3">
        <TextEdit
          onInput={(department) => setInfo({ ...info, department })}
          value={info.department}
          placeholder="Кафедра"
        />
      </div>
      <div class="mt-3">
        <TextEdit
          onInput={(student_name) => setInfo({ ...info, student_name })}
          value={info.student_name}
          placeholder="Имя студента"
        />
      </div>
      <div class="mt-3">
        <TextEdit
          onInput={(group_number) => setInfo({ ...info, group_number })}
          value={info.group_number}
          placeholder="Номер группы"
        />
      </div>
      <div class="mt-3">
        <TextEdit
          onInput={(record_book_number) =>
            setInfo({ ...info, record_book_number })
          }
          value={info.record_book_number}
          placeholder="Номер зачётной книжки"
        />
      </div>
      <div class="mt-4 flex justify-end">
        <Button onClick={handleSubmit}>Сохранить</Button>
      </div>
    </Modal>
  );
};
