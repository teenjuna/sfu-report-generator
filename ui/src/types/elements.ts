export enum SegmentElementType {
  Image = 'Image',
  Paragraph = 'Paragraph',
  CodeSnippet = 'CodeSnippet',
  Enumeration = 'Enumeration',
}

export class SegmentElement {
  type: SegmentElementType;

  constructor(type: SegmentElementType) {
    this.type = type;
  }
}

export class Paragraph extends SegmentElement {
  contents: string;

  constructor(contents: string) {
    super(SegmentElementType.Paragraph);
    this.contents = contents;
  }
}

export class Image extends SegmentElement {
  uri: string;
  caption: string;

  constructor(uri: string, caption: string) {
    super(SegmentElementType.Image);
    this.uri = uri;
    this.caption = caption;
  }
}

export class CodeSnippet extends SegmentElement {
  contents: string;
  caption: string;

  constructor(contents: string, caption: string) {
    super(SegmentElementType.CodeSnippet);
    this.contents = contents;
    this.caption = caption;
  }
}

export class Enumeration extends SegmentElement {
  contents: string[];

  constructor(contents: string[]) {
    super(SegmentElementType.Enumeration);
    this.contents = contents;
  }
}
