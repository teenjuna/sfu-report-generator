import type { Chapter } from './segments';

export type Report = {
  info: ReportInfo;
  chapters: Chapter[];
};

export type ReportInfo = {
  theme: string;
  teacher_name: string;
  student_name: string;
  institute: string;
  department: string;
  group_number: string;
  record_book_number: string;
};
