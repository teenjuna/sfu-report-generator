import { Component, createEffect, onCleanup } from 'solid-js';
import { HomePage, EditorPage } from './pages';
import { Router, Route } from 'solid-app-router';

export const Page: Component = () => {
  let ref: HTMLDivElement;

  const setHeight = () => {
    ref.style.height = `${window.innerHeight}px`;
  };

  createEffect(() => {
    setHeight();
    window.addEventListener('resize', setHeight);
  });

  onCleanup(() => window.removeEventListener('resize', setHeight));

  return (
    <div ref={ref} class="p-3 bg-gray-300">
      <Router routes={routes}>
        <Route />
      </Router>
    </div>
  );
};

const routes = [
  { path: '/', component: HomePage },
  { path: '/editor', component: EditorPage },
];
