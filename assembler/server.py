import argparse
import orjson
import subprocess
import os
import pathlib
import binascii
from base64 import b64decode
from hashlib import md5
from tempfile import gettempdir
from flask import Flask, request, send_file
from flask_cors import CORS
from src.document import Document

parser = argparse.ArgumentParser(description="Run the server")
parser.add_argument(
    "--docxgen-dir", type=str, required=True, help="path to the docxgen directory"
)
parser.add_argument(
    "--host", type=str, default="0.0.0.0", help="the address of the server"
)
parser.add_argument(
    "--port",
    type=int,
    default=int(os.environ.get("PORT", 5000)),
    help="the port of the server",
)
args = parser.parse_args()

tmpdir = os.path.join(gettempdir(), "docxgen")
pathlib.Path(tmpdir).mkdir(exist_ok=True)

app = Flask(__name__)
CORS(app)


@app.route("/generate", methods=["GET"])
def assemble():
    # Get JSON data from the request.
    try:
        data = request.args["data"]
    except KeyError:
        return "data is not provided", 400
    try:
        data = b64decode(data.replace(" ", "+")).decode("utf-8")
    except binascii.Error:
        return "data is incorrectly encoded", 400

    # Generate flat representation of data.
    try:
        doc = Document.from_json(data)
        flat = orjson.dumps(doc.flatten())
    except Exception as err:
        print("failed to flatten", err)
        return "data is invalid", 400

    return app.response_class(response=flat, status=200, mimetype="application/json")


@app.route("/generate-docx", methods=["GET"])
def generate_docx():
    # Get JSON data from the request.
    try:
        data = request.args["data"]
    except KeyError:
        return "data is not provided", 400
    try:
        data = b64decode(data.replace(" ", "+")).decode("utf-8")
    except binascii.Error:
        return "data is incorrectly encoded", 400

    # Generate flat representation of data.
    try:
        doc = Document.from_json(data)
        flat = orjson.dumps(doc.flatten())
    except Exception as err:
        print("failed to flatten", err)
        return "data is invalid", 400

    flat_hash = f"tmp{md5(flat).hexdigest()}"
    json_file = os.path.join(tmpdir, f"{flat_hash}.json")
    docx_file = os.path.join(tmpdir, f"{flat_hash}.docx")

    # Write flat representation to a file.
    with open(json_file, "w") as f:
        f.write(flat.decode("utf-8"))

    # Generate .docx file from generated flat representation.
    print("| Generating .docx file")
    process = subprocess.Popen(
        ["npm", "run", "gen", json_file, docx_file], cwd=args.docxgen_dir
    )
    process.wait()
    if os.path.isfile(docx_file):
        print("| File successfully generated")
    else:
        print("| File generation failed")

    # Send the file to the client.
    print("| Sending file")
    return send_file(
        docx_file,
        as_attachment=True,
        mimetype="application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    )


app.run(args.host, args.port)
