from .typographic_constants import *


class Position:
    def __init__(self):
        self.offset = 0
        self.spacing_before = 0
        self.page = 1

    def new_page(self):
        self.page += 1
        self.offset = 0


class Numerator:
    def __init__(self):
        self.chapter = 1
        self.subchapter = 1
        self.section = 1
        self.subsection = 1
        self.code_snippet = 1
        self.table = 1
        self.image = 1

    def new_chapter(self):
        self.chapter += 1
        self.subchapter = self.section = self.subsection = 1

    def new_subchapter(self):
        self.subchapter += 1
        self.section = self.subsection = 1

    def new_section(self):
        self.section += 1
        self.subsection = 1

    def new_subsection(self):
        self.subsection += 1

    def new_snippet(self):
        self.code_snippet += 1

    def new_table(self):
        self.table += 1

    def new_image(self):
        self.image += 1


TNR = 0
TNR_BD = 1
C_NEW = 2


def string_length(string: str, font=TNR):
    """
    Calculates the length of a line depending on the font
    """
    acc = 0
    if font == TNR:
        for ch in string:
            if ord(ch) in sizes:
                acc += sizes[ord(ch)]
    if font == TNR_BD:
        for ch in string:
            if ord(ch) in sizes:
                acc += sizes_bd[ord(ch)]
    if font == C_NEW:
        tabs = string.count("\t")
        return tabs * TAB_C_NEW_SIZE + (len(string) - tabs) * LITERA_C_NEW_SIZE
    return acc


def split_string(string: str, field, text_size, font=TNR):
    """
    Splitting a string into lines and words
    """
    splitted = string.split()
    if not splitted:
        return [[" "]]
    if string.startswith('\t'):
        splitted[0] = '\t' + splitted[0]
    rows = [[]]
    cur_row_len = 0
    for word in splitted:
        if cur_row_len + text_size * string_length(word, font) >= field:
            # if word longer than page width
            if text_size * string_length(word, font) >= field:
                strings = [[""]]
                # current string width
                cur_size = 0
                for litera in word:
                    if text_size * string_length(litera, font) + cur_size >= field:
                        strings.append([""])
                        cur_size = 0
                    strings[-1][0] += litera
                    cur_size += string_length(litera, font) * text_size
                rows += strings
                cur_row_len = cur_size
            # if word <= than page width
            else:
                rows.append([word])
                # update position
                cur_row_len = text_size * (string_length(word, font) + string_length(' ', font))
        else:
            # append last string
            rows[-1].append(word)
            # update position
            cur_row_len += text_size * (string_length(word, font) + string_length(' ', font))
        rows = [rows[i] for i in range(len(rows)) if len(rows[i])]
    return rows


def convert_millimeters(millimeters):
    """
    Convert millimeters into 1/1000pt
    """
    return int(2834 * millimeters)


def split_strings(strings, fields, text_size, font=TNR, borders=0):
    """
    Applies split string for each string in strings
    """
    result = []
    for i in range(len(strings)):
        result.append(split_string(strings[i], convert_millimeters(fields[i]) - borders, text_size, font))
    return result


def cut_string(string, field, text_size, font=TNR):
    acc = 0
    for i in range(len(string)):
        if font == TNR:
            if ord(string[i]) in sizes:
                acc += sizes[string[i]] * text_size
                if acc >= field:
                    return string[:i]
        if font == TNR_BD:
            if ord(string[i]) in sizes_bd:
                acc += sizes_bd[string[i]] * text_size
                if acc >= field:
                    return string[:i]
        if font == C_NEW:
            if string[i] != '\t':
                acc += 2400 * text_size
            else:
                acc += 600 * text_size
            if acc >= field:
                return string[:i]
    return string


def split_by_page_break(elements):
    if not elements:
        return []
    pages = [[]]
    while elements:
        element = elements.pop(0)
        if element["type"] == "pb":
            pages.append([])
        if element["type"] == "text" or element["type"] == "enumeration":
            pages[-1].append({"type": element["type"], "contents": [" ".join(zipped) for zipped in element["text"]],
                              "require_tab": element["require_tab"], "sb": element["sb"],
                              "sb_percentage": int(element["sb"] / 140)})
        if element["type"] == "chapter_heading":
            pages[-1].append({"type": "heading", "title": [" ".join(zipped) for zipped in element["text"]],
                              "sb": element["sb"], "sb_percentage": int(element["sb"] / 140)})
        if element["type"] == "image":
            element["caption"] = [" ".join(zipped) for zipped in element["caption"]]
            element["sb_percentage"] = int(element["sb"] / 140)
            pages[-1].append(element)
        if element["type"] == "pre_table_text":
            nxt = elements.pop(0)
            if nxt["type"] == "listing":
                pages[-1].append({"type": "snippet", "sb": element["sb"], "sb_percentage": int(element["sb"] / 140),
                                  "caption": [" ".join(zipped) for zipped in element["text"]],
                                  "contents": nxt["data"]})
            if nxt["type"] == "table":
                pages[-1].append({"type": "table", "sb": element["sb"], "width_millimeters": nxt["sizes"],
                                  "width_percentages": [int(p / 170 * 100) for p in nxt["sizes"]],
                                  "caption": [" ".join(zipped) for zipped in element["text"]],
                                  "size": [len(nxt["data"][0]), len(nxt["data"])],
                                  "sb_percentage": int(element["sb"] / 140)})
                rows = nxt["data"]
                for i in range(len(rows)):
                    for j in range(len(rows[i])):
                        rows[i][j] = [" ".join(zipped) for zipped in rows[i][j]]
                pages[-1][-1]["rows"] = rows
    return pages
