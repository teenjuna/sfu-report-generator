import orjson
from .elements import *


class EmptyTitleError(Exception):
    pass


class AmbiguousSegmentContentError(Exception):
    pass


class Segment:
    def to_json(self):
        return orjson.dumps(self, default=lambda o: o.__dict__)


class Chapter(Segment):
    def __init__(self, title, elements, subchapters):
        assert isinstance(title, str), "title must be str"
        assert isinstance(elements, list), "elements must be list"
        for el in elements:
            assert isinstance(el, Element), "elements[i] must be Element"
        assert isinstance(subchapters, list), "subchapters must be list"
        for sbc in subchapters:
            assert isinstance(sbc, Subchapter), "subchapters[i] must be Subchapter"
        title = title.strip()
        if title == "":
            raise EmptyTitleError
        if len(elements) != 0 and len(subchapters) != 0:
            raise AmbiguousSegmentContentError
        self.title = title
        self.elements = elements
        self.subchapters = subchapters

    def __repr__(self):
        return f'Chapter({self.title})'

    @classmethod
    def from_json(cls, obj):
        if isinstance(obj, (bytes, bytearray, str)):
            obj = orjson.loads(obj)
        title = obj["title"]
        elements = [Element.from_json(obj) for obj in obj["elements"]]
        subchapters = [Subchapter.from_json(obj) for obj in obj["subchapters"]]
        return cls(title, elements, subchapters)

    def flatten(self, out: list, table_of_contents_data: list, num: Numerator, pos: Position):
        # checking required spacing before
        spacing_before = max(pos.spacing_before, HEADING_BEFORE) if num.chapter != 1 else 0
        # update table of contents data
        chapter_name = split_string('\t' + str(num.chapter) + ' ' + self.title, convert_millimeters(170), 14,
                                    font=TNR_BD)[:MAX_HEADER_STRINGS]
        # fix tabulation
        chapter_name[0][0] = chapter_name[0][0][1:]
        if TNR_14PT_HEIGHT * len(chapter_name) + spacing_before >= PAGE_HEIGHT - pos.offset:
            out.append({"type": "pb"})
            pos.new_page()
            out.append({"type": "chapter_heading", "text": chapter_name, "sb": 0})
            pos.offset += TNR_14PT_HEIGHT * len(chapter_name)
        else:
            out.append({"type": "chapter_heading", "text": chapter_name, "sb": spacing_before})
            pos.offset += spacing_before + TNR_14PT_HEIGHT * len(chapter_name)
        pos.spacing_before = HEADING_AFTER
        table_of_contents_data.append((self.title, pos.page, num.chapter))
        num.new_chapter()
        if self.elements:
            for el in self.elements:
                el.flatten(out, num, pos)
        else:
            for s in self.subchapters:
                s.flatten(out, table_of_contents_data, num, pos)


class Subchapter(Segment):
    def __init__(self, title, elements, sections):
        assert isinstance(title, str), "title must be str"
        assert isinstance(elements, list), "elements must be list"
        for el in elements:
            assert isinstance(el, Element), "elements[i] must be Element"
        assert isinstance(sections, list), "sections must be list"
        for sec in sections:
            assert isinstance(sec, Section), "sections[i] must be Section"
        title = title.strip()
        self.title = title
        self.elements = elements
        self.sections = sections

    def __repr__(self):
        return f'Subchapter({self.title}, [{",".join(self.elements)}])'

    @classmethod
    def from_json(cls, obj):
        if isinstance(obj, (bytes, bytearray, str)):
            obj = orjson.loads(obj)
        title = obj["title"]
        elements = [Element.from_json(obj) for obj in obj["elements"]]
        sections = [Section.from_json(obj) for obj in obj["sections"]]
        return cls(title, elements, sections)

    def flatten(self, out: list, table_of_contents_data: list, num: Numerator, pos: Position):
        spacing_before = max(pos.spacing_before, HEADING_BEFORE)
        subchapter_name = split_string('\t' + str(num.chapter) + '.' + str(num.subchapter) + ' ' + self.title,
                                       convert_millimeters(170), 14, font=TNR_BD)[:MAX_HEADER_STRINGS]
        # fix tabulation
        subchapter_name[0][0] = subchapter_name[0][0][1:]
        if spacing_before + TNR_14PT_HEIGHT * len(subchapter_name) >= PAGE_HEIGHT - pos.offset:
            out.append({"type": "pb"})
            pos.new_page()
            out.append({"type": "chapter_heading", "text": subchapter_name, "sb": 0})
            pos.offset += TNR_14PT_HEIGHT * len(subchapter_name)
        else:
            out.append({"type": "chapter_heading", "text": subchapter_name, "sb": spacing_before})
            pos.offset += TNR_14PT_HEIGHT * len(subchapter_name) + spacing_before
        pos.spacing_before = HEADING_AFTER
        table_of_contents_data.append((self.title, pos.page, num.chapter, num.subchapter))
        num.new_subchapter()
        if self.elements:
            for el in self.elements:
                el.flatten(out, num, pos)
        else:
            for s in self.sections:
                s.flatten(out, table_of_contents_data, num, pos)


class Section(Segment):
    def __init__(self, title, elements, subsections):
        assert isinstance(title, str), "title must be str"
        assert isinstance(elements, list), "elements must be list"
        for el in elements:
            assert isinstance(el, Element), "elements[i] must be Element"
        assert isinstance(subsections, list), "subsections must be list"
        for sbs in subsections:
            assert isinstance(sbs, Subsection), "subsections[i] must be Subsection"
        title = title.strip()
        self.title = title
        self.elements = elements
        self.subsections = subsections

    def __repr__(self):
        return f"Section({self.title})"

    @classmethod
    def from_json(cls, obj):
        if isinstance(obj, (bytes, bytearray, str)):
            obj = orjson.loads(obj)
        title = obj["title"]
        elements = [Element.from_json(obj) for obj in obj["elements"]]
        subsections = [Subsection.from_json(obj) for obj in obj["subsections"]]
        return cls(title, elements, subsections)

    def flatten(self, out: list, table_of_contents_data: list, num: Numerator, pos: Position):
        spacing_before = max(pos.spacing_before, HEADING_BEFORE)
        section_name = split_string(f"\t{num.chapter}.{num.subchapter}.{num.section} {self.title}",
                                    convert_millimeters(170), 14, font=TNR_BD)[:MAX_HEADER_STRINGS]
        # fix tabulation
        section_name[0][0] = section_name[0][0][1:]
        if spacing_before + TNR_14PT_HEIGHT * len(section_name) >= PAGE_HEIGHT - pos.offset:
            out.append({"type": "pb"})
            pos.new_page()
            out.append({"type": "chapter_heading", "text": section_name, "sb": 0})
            pos.offset += TNR_14PT_HEIGHT * len(section_name)
        else:
            out.append({"type": "chapter_heading", "text": section_name, "sb": spacing_before})
            pos.offset += spacing_before + TNR_14PT_HEIGHT * len(section_name)
        pos.spacing_before = HEADING_AFTER
        table_of_contents_data.append((self.title, pos.page, num.chapter, num.subchapter, num.section))
        num.new_section()
        if self.elements:
            for el in self.elements:
                el.flatten(out, num, pos)
        else:
            for subs in self.subsections:
                subs.flatten(out, table_of_contents_data, num, pos)


class Subsection(Segment):
    def __init__(self, title, elements):
        assert isinstance(title, str), "title must be str"
        assert isinstance(elements, list), "elements must be list"
        for el in elements:
            assert isinstance(el, Element), "elements[i] must be Element"
        title = title.strip()
        self.title = title
        self.elements = elements

    def __repr__(self):
        return f"Subsection({self.title})"

    @classmethod
    def from_json(cls, obj):
        if isinstance(obj, (bytes, bytearray, str)):
            obj = orjson.loads(obj)
        title = obj["title"]
        elements = [Element.from_json(obj) for obj in obj["elements"]]
        return cls(title, elements)

    def flatten(self, out: list, table_of_contents_data: list, num: Numerator, pos: Position):
        spacing_before = max(pos.spacing_before, HEADING_BEFORE)
        subsection_name = split_string(f"\t{num.chapter}.{num.subchapter}.{num.section}.{num.subsection} {self.title}",
                                       convert_millimeters(170), 14, font=TNR_BD)[:MAX_HEADER_STRINGS]
        # fix tabulation
        subsection_name[0][0] = subsection_name[0][0][1:]
        if spacing_before + TNR_14PT_HEIGHT * len(subsection_name) >= PAGE_HEIGHT - pos.offset:
            out.append({"type": "pb"})
            pos.new_page()
            out.append({"type": "chapter_heading", "text": subsection_name, "sb": 0})
            pos.offset += TNR_14PT_HEIGHT
        else:
            out.append({"type": "chapter_heading", "text": subsection_name, "sb": spacing_before})
            pos.offset += spacing_before + TNR_14PT_HEIGHT * len(subsection_name)
        pos.spacing_before = HEADING_AFTER
        table_of_contents_data.append((self.title, pos.page, num.chapter, num.subchapter, num.section, num.subsection))
        num.new_subsection()
        for el in self.elements:
            el.flatten(out, num, pos)


class TableOfContents:
    def __init__(self, data: list):
        self.data = data

    def flatten(self):
        left_parts = []
        central_parts = []
        pbs = set()
        for toc_obj in self.data:
            left_part = ".".join([str(_) for _ in toc_obj[2:]]) + (' ' if len(toc_obj) > 2 else '')
            left_parts.append(left_part)
            central_part = split_string(toc_obj[0], convert_millimeters(170) - string_length(left_part) * 14 - 28000,
                                        14)
            central_parts.append([" ".join(_) for _ in central_part])
        cur_page_rem = 41
        for i in range(len(central_parts)):
            split = central_parts[i]
            if cur_page_rem - len(split) < 0:
                cur_page_rem = 42
                pbs.add(i)
            cur_page_rem -= len(split)
        total_pages_before_toc = 2 + len(pbs)
        for i in range(len(central_parts)):
            cp = central_parts[i]
            cp[0] = left_parts[i] + cp[0]
            for j in range(1, len(cp)):
                cp[j] = ' ' * (string_length(left_parts[i]) // SPACE_SIZE) + cp[j]
            dots = (convert_millimeters(170) - string_length(cp[-1]) * 14 - 28000) // 3500
            number = total_pages_before_toc + self.data[i][1]
            cp[-1] += '.' * dots + '\t' + str(number)
        result = {"paragraphs": []}
        for i in range(len(central_parts)):
            result["paragraphs"].append({"req_pb": i in pbs, "text": central_parts[i]})
        return result
