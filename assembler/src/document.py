from .segments import *


class Document:
    def __init__(self, chapters, info=None):
        self.chapters = chapters
        self.info = info

    @classmethod
    def from_json(cls, obj):
        if isinstance(obj, (bytes, bytearray, str)):
            obj = orjson.loads(obj)
        chapters = [Chapter.from_json(obj) for obj in obj["chapters"]]
        if "info" in obj:
            assert "theme" in obj["info"] and "teacher_name" in obj["info"] and "student_name" in obj["info"] and \
                   "institute" in obj["info"] and "department" in obj["info"] and "group_number" in obj["info"] and \
                   "record_book_number" in obj["info"] and isinstance(obj["info"]["theme"], str) and\
                   isinstance(obj["info"]["teacher_name"], str) and isinstance(obj["info"]["student_name"], str) and \
                   isinstance(obj["info"]["institute"], str) and isinstance(obj["info"]["department"], str) and\
                   isinstance(obj["info"]["group_number"], str) and isinstance(obj["info"]["record_book_number"], str),\
                "all fields of info must be str"
            return cls(chapters, obj["info"])
        return cls(chapters)

    def flatten(self):
        json_accomulator = {}
        if self.info is not None:
            json_accomulator["info"] = self.info
        table_of_contents_data = []
        pos = Position()
        num = Numerator()
        main_part_elements = []
        for ch in self.chapters:
            ch.flatten(main_part_elements, table_of_contents_data, num, pos)
        pages = split_by_page_break(main_part_elements)
        json_accomulator["main_part"] = {"pages": pages}
        json_accomulator["table_of_contents"] = TableOfContents(table_of_contents_data).flatten()
        return json_accomulator
