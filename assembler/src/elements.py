import orjson
import rfc3986
from .helpers import *
from PIL import Image as IMG
import requests


class InvalidElementTypeError(Exception):
    pass


class EmptyContentsError(Exception):
    pass


class InvalidUriError(Exception):
    pass


class TooLargeTable(Exception):
    pass


class TooLargeImage(Exception):
    pass


class Element:
    def __init__(self, element_type):
        if element_type not in ("Paragraph", "Image", "Table", "CodeSnippet", "Formula", "Enumeration"):
            raise InvalidElementTypeError
        self.element_type = element_type

    @staticmethod
    def from_json(obj):
        if isinstance(obj, (bytes, bytearray, str)):
            obj = orjson.loads(obj)
        if obj["type"] == "Paragraph":
            return Paragraph.from_json(obj)
        if obj["type"] == "Image":
            return Image.from_json(obj)
        if obj["type"] == "Table":
            return Table.from_json(obj)
        if obj["type"] == "CodeSnippet":
            return CodeSnippet.from_json(obj)
        if obj["type"] == "Formula":
            return Formula.from_json(obj)
        if obj["type"] == "Enumeration":
            return Enumeration.from_json(obj)
        raise InvalidElementTypeError


class Paragraph(Element):
    def __init__(self, contents):
        super().__init__("Paragraph")
        assert isinstance(contents, str), "contents must be str"
        contents = contents.strip()
        if contents == '':
            raise EmptyContentsError
        self.contents = contents

    def __repr__(self):
        return f'Paragraph({self.contents})'

    @classmethod
    def from_json(cls, obj):
        if isinstance(obj, (bytes, bytearray, str)):
            obj = orjson.loads(obj)
        contents = obj["contents"]
        return cls(contents)

    def flatten(self, out: list, num: Numerator, pos: Position):
        spacing_before = max(pos.spacing_before, TEXT_BEFORE)
        i = 0
        splitted_text = split_string('\t' + self.contents, convert_millimeters(170), 14)
        splitted_text[0][0] = splitted_text[0][0][1:]
        accomulator = []
        for string in splitted_text:
            if TNR_14PT_HEIGHT + pos.offset + (spacing_before if i == 0 else 0) >= PAGE_HEIGHT:
                if accomulator:
                    out.append({"type": "text", "text": accomulator, "sb": (0 if i != 0 else spacing_before),
                                "require_tab": i == 0})
                out.append({"type": "pb"})
                pos.new_page()
                accomulator = []
                i += 1
            accomulator.append(string)
            pos.offset += TNR_14PT_HEIGHT
        out.append({"type": "text", "text": accomulator, "sb": (spacing_before if i == 0 else 0), "require_tab": i == 0})
        pos.offset += (spacing_before if i == 0 else 0)
        pos.spacing_before = TEXT_AFTER


class Image(Element):
    def __init__(self, uri, caption):
        super().__init__("Image")
        assert isinstance(uri, str), "uri must be str"
        assert isinstance(caption, str), "caption must be str"
        caption = caption.strip()
        uri = uri.strip()
        if caption == '' or uri == '':
            raise EmptyContentsError
        if not rfc3986.is_valid_uri(uri) or rfc3986.urlparse(uri).scheme not in ("https", "file"):
            raise InvalidUriError
        if caption.endswith('.'):
            caption = caption[:-1]
        self.uri = uri
        self.caption = caption.capitalize()

    def __repr__(self):
        return f"Image({self.uri}, {self.caption})"

    @classmethod
    def from_json(cls, obj):
        if isinstance(obj, (bytes, bytearray, str)):
            obj = orjson.loads(obj)
        uri = obj["uri"]
        caption = obj["caption"]
        return cls(uri, caption)

    def flatten(self, out: list, num: Numerator, pos: Position):
        spacing_before = max(pos.spacing_before, PICTURE_BEFORE)
        target_width = 400
        resp = requests.get(self.uri, stream=True).raw
        image = IMG.open(resp)
        width, height = image.size
        original_aspect_ratio = width / height
        dest_width = target_width
        dest_height = int(target_width / original_aspect_ratio)
        description = split_string(f"Рисунок {num.image} – {self.caption}", convert_millimeters(170), 14)
        construction_size = len(description) * TNR_14PT_HEIGHT + dest_height * PUNCTS_PER_PIXEL + PICTURE_TEXT_BEFORE \
                            + spacing_before + PICTURE_AFTER
        if construction_size - spacing_before >= PAGE_HEIGHT:
            raise TooLargeImage
        if construction_size >= PAGE_HEIGHT - pos.offset:
            out.append({"type": "pb"})
            pos.new_page()
            pos.offset += construction_size - spacing_before
            out.append({"type": "image", "size": (dest_width, dest_height), "uri": self.uri,
                        "caption": description, "sb": 0})
        else:
            pos.offset += construction_size
            out.append({"type": "image", "size": (dest_width, dest_height), "uri": self.uri,
                        "caption": description, "sb": spacing_before})
        pos.spacing_before = PICTURE_TEXT_AFTER
        num.new_image()


class Table(Element):
    def __init__(self, caption, size, rows):
        super().__init__("Table")
        assert isinstance(caption, str), "caption must be str"
        assert caption, "caption must'nt be empty"
        self.caption = caption
        assert isinstance(size, list) and len(size) == 2 and isinstance(size[0], int) and isinstance(size[1], int), \
            "size must be list[int, int]"
        self.rows_count, self.columns_count = size
        assert isinstance(rows, list), "rows must be list"
        assert len(rows) == self.rows_count, "rows must match size"
        for row in rows:
            assert isinstance(row, list), "row in rows must be list"
            assert len(row) == self.columns_count, "columns must match size"
            for element in row:
                assert isinstance(element, str), "element of row must be str"
        assert self.rows_count <= 10, "columns must be <= 10"
        self.width = int(170 / self.columns_count)
        self.rows = rows

    def __repr__(self):
        return f"Table({self.caption}, {self.rows_count}, {self.columns_count})"

    @classmethod
    def from_json(cls, obj):
        if isinstance(obj, (bytes, bytearray, str)):
            obj = orjson.loads(obj)
        caption = obj["caption"]
        size = obj["size"]
        rows = obj["rows"]
        return cls(caption, size, rows)

    def flatten(self, out: list, num: Numerator, pos: Position):
        spacing_before = max(pos.spacing_before, PRE_TABLE_TEXT_BEFORE)
        free_space = PAGE_HEIGHT - pos.offset
        header = self.rows[0]
        rows = self.rows[1:]
        header_height = max([len(split_string(header[i], convert_millimeters(self.width), 12)) * TNR_12PT_HEIGHT
                             for i in range(len(header))])
        rows_height = []
        for i in range(len(rows)):
            rows_height.append(max([len(split_string(rows[i][j], convert_millimeters(self.width), 12)) * TNR_12PT_HEIGHT
                                    for j in range(self.columns_count)]))
        pre_table_text = split_string("Таблица " + str(num.table) + ' - ' + self.caption, convert_millimeters(170), 14)
        if header_height + rows_height[0] + 3 * BORDER_TABLE_SIZE + len(pre_table_text) * TNR_14PT_HEIGHT + \
                PRE_TABLE_TEXT_AFTER >= PAGE_HEIGHT or header_height + max(rows_height) + 3 * BORDER_TABLE_SIZE + \
                TNR_14PT_HEIGHT + PRE_TABLE_TEXT_AFTER >= PAGE_HEIGHT:
            raise TooLargeTable
        first_pb = False
        if header_height + rows_height[0] + 3 * BORDER_TABLE_SIZE + len(
                pre_table_text) * TNR_14PT_HEIGHT + PRE_TABLE_TEXT_AFTER + spacing_before >= PAGE_HEIGHT - pos.offset:
            out.append({"type": "pb"})
            pos.new_page()
            first_pb = True
        out.append({"type": "pre_table_text", "text": pre_table_text, "sb": (0 if first_pb else spacing_before)})
        rows_accomulator = [split_strings(header, [self.width for _ in range(self.columns_count)], 12),
                            split_strings(rows[0], [self.width for _ in range(self.columns_count)], 12)]
        pos.offset += header_height + rows_height[0] + len(pre_table_text) * TNR_14PT_HEIGHT + PRE_TABLE_TEXT_AFTER + \
                      (0 if first_pb else spacing_before) + 3 * BORDER_TABLE_SIZE
        table_continue = [["Продолжение", "таблицы", str(num.table)]]
        for i in range(1, len(rows)):
            if rows_height[i] + pos.offset >= PAGE_HEIGHT:
                if rows_accomulator:
                    out.append({"type": "table", "data": rows_accomulator,
                                "sizes": [self.width for _ in range(self.columns_count)]})
                out.append({"type": "pb"})
                pos.new_page()
                out.append({"type": "pre_table_text", "text": table_continue, "sb": 0})
                rows_accomulator = [split_strings(header, [self.width for _ in range(self.columns_count)], 12)]
                pos.offset = TNR_14PT_HEIGHT * len(table_continue) + PRE_TABLE_TEXT_AFTER + header_height + \
                             2 * BORDER_TABLE_SIZE
            rows_accomulator.append(split_strings(rows[i], [self.width for _ in range(self.columns_count)], 12))
            pos.offset += rows_height[i] + BORDER_TABLE_SIZE
        if rows_accomulator:
            out.append({"type": "table", "data": rows_accomulator, "sizes":
                [self.width for _ in range(self.columns_count)]})
        for element in reversed(out):
            if element["type"] == "pre_table_text":
                if element["text"] == table_continue:
                    element["text"] = [["Окончание", "таблицы", str(num.table)]]
                break
        num.new_table()
        pos.spacing_before = TABLE_AFTER


class CodeSnippet(Element):
    def __init__(self, caption, contents):
        super().__init__("CodeSnippet")
        assert isinstance(caption, str), "caption must be str"
        assert isinstance(contents, str), "contents must be str"
        caption = caption.strip().capitalize()
        contents = contents.strip()
        if caption == '' or contents == '':
            raise EmptyContentsError
        self.caption = caption
        self.contents = contents

    def __repr__(self):
        return f"CodeSnippet({self.caption}, {self.contents})"

    @classmethod
    def from_json(cls, obj):
        if isinstance(obj, (bytes, bytearray, str)):
            obj = orjson.loads(obj)
        caption = obj["caption"]
        contents = obj["contents"]
        return cls(caption, contents)

    def flatten(self, out: list, num: Numerator, pos: Position):
        spacing_before = max(pos.spacing_before, LISTING_BEFORE)
        caption = split_string(f"Листинг {num.code_snippet} - {self.caption}", convert_millimeters(170), 14)
        logical_rows = self.contents.replace('\t', '    ').split('\n')
        # removing empty strings in tail
        while logical_rows[-1] == '' and len(logical_rows):
            logical_rows.pop(-1)
        # split all code into rows
        rows = []
        for row in logical_rows:
            spaces_before = 0
            for i in range(len(row)):
                if row[i] != ' ':
                    break
                else:
                    spaces_before += 1
            # do not insert str, that contains only spaces
            if spaces_before == len(row):
                continue
            appendix = [' ' * spaces_before + " ".join(s) for s in
                        split_string(row, convert_millimeters(170) - spaces_before * LITERA_C_NEW_SIZE * 10, 10, C_NEW)]
            rows += appendix
        first_pb = False
        if C_NEW_10PT_HEIGHT + len(caption) * TNR_14PT_HEIGHT + PRE_LISTING_TEXT_AFTER + spacing_before >= \
                PAGE_HEIGHT - pos.offset:
            out.append({"type": "pb"})
            pos.new_page()
            first_pb = True
        out.append({"type": "pre_table_text", "text": caption, "sb": (0 if first_pb else spacing_before)})
        rows_accomulator = [rows[0]]
        pos.offset += C_NEW_10PT_HEIGHT + len(caption) * TNR_14PT_HEIGHT + PRE_LISTING_TEXT_AFTER + (0 if first_pb else
                                                                                                     spacing_before)
        code_snippet_continue = [["Продолжение", "листинг", str(num.code_snippet)]]
        for i in range(1, len(rows)):
            if C_NEW_10PT_HEIGHT + pos.offset >= PAGE_HEIGHT:
                if rows_accomulator:
                    out.append({"type": "listing", "data": rows_accomulator})
                rows_accomulator = []
                out.append({"type": "pb"})
                pos.new_page()
                out.append({"type": "pre_table_text", "text": code_snippet_continue, "sb": 0})
                pos.offset = TNR_14PT_HEIGHT * len(caption) + PRE_LISTING_TEXT_AFTER
            rows_accomulator.append(rows[i])
            pos.offset += C_NEW_10PT_HEIGHT
        if rows_accomulator:
            out.append({"type": "listing", "data": rows_accomulator})
        for element in reversed(out):
            if element["type"] == "pre_table_text":
                if element["text"] == code_snippet_continue:
                    element["text"] = [["Окончание", "листинга", str(num.code_snippet)]]
                break
        pos.spacing_before = LISTING_AFTER
        num.new_snippet()


class Formula(Element):
    def __init__(self, contents):
        super().__init__("Formula")
        assert isinstance(contents, str), "contents must be str"
        contents = contents.strip()
        if contents == '':
            raise EmptyContentsError
        self.contents = contents

    def __repr__(self):
        return f"Formula({self.contents})"

    @classmethod
    def from_json(cls, obj):
        if isinstance(obj, (bytes, bytearray, str)):
            obj = orjson.loads(obj)
        contents = obj["contents"]
        return cls(contents)


class Enumeration(Element):
    def __init__(self, contents):
        super().__init__("Enumeration")
        assert isinstance(contents, list), "contents must be list"
        assert contents, "contents mustn't be empty"
        for item in contents:
            assert isinstance(item, str), "contents must include only str"
        self.contents = contents

    def __repr__(self):
        return f"Enumeration({self.contents})"

    @classmethod
    def from_json(cls, obj):
        if isinstance(obj, (bytes, bytearray, str)):
            obj = orjson.loads(obj)
        contents = obj["contents"]
        return cls(contents)

    def flatten(self, out: list, num: Numerator, pos: Position):
        spacing_before = max(pos.spacing_before, ENUM_BEFORE)
        strings = self.contents
        for i in range(len(strings) - 1):
            strings[i] = '\t- ' + strings[i] + ';'
        strings[-1] = '\t- ' + strings[-1] + '.'
        enum_elements = []
        for string in strings:
            enum_elements.append(split_string(string, convert_millimeters(170), 14, TNR))
        for i, enum_element in enumerate(enum_elements):
            enum_element[0][0] = enum_element[0][0][1:]
            if pos.offset + len(enum_element) * TNR_14PT_HEIGHT + (spacing_before if i == 0 else 0) >= PAGE_HEIGHT:
                out.append({"type": "pb"})
                pos.new_page()
                out.append({"type": "enumeration", "text": enum_element, "sb": 0, "require_tab": True})
                pos.offset = len(enum_element) * TNR_14PT_HEIGHT
            else:
                out.append({"type": "enumeration", "text": enum_element, "sb": (spacing_before if i == 0 else 0),
                            "require_tab": True})
                pos.offset += len(enum_element) * TNR_14PT_HEIGHT + (spacing_before if i == 0 else 0)
        pos.spacing_before = ENUM_AFTER
